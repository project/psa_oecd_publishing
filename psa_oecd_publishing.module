<?php

/**
 * @file
 * Contains psa_oecd_publishing.module.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Implements hook_help().
 */
function psa_oecd_publishing_help(string $route_name, RouteMatchInterface $route_match): string {
  switch ($route_name) {
    case 'help.page.psa_oecd_publishing':
      $output = '';
      $output .= '<h3>' . t('OECD GlobalRecalls API') . '</h3>';
      $output .= '<p>' . t('Refer to README.md.') . '</p>';
      return $output;
  }

  return '';
}

/**
 * Implements hook_ENTITY_TYPE_presave() for node.
 *
 * This runs when a recall node is saved. It sets the flag that says that this
 * node needs to be updated in the OECD GlobalRecalls portal. When the update
 * happens, this flag is un-set in OecdPublishingController::updateNodeStatus().
 *
 * The parameter needs to be NodeInterface not FieldableEntityInterface because
 * it uses revisions. There is no FieldableEntityInterface::getRevisionId().
 */
function psa_oecd_publishing_node_presave(NodeInterface $node): void {
  $config = \Drupal::config('psa_oecd_publishing.settings');

  $recall_data_content_type = $config->get('recall_data_content_type');
  if ($node->bundle() === $recall_data_content_type) {
    if (_psa_oecd_publishing_node_needs_oecd_publish($node)) {
      $oecd_publish_needed_field = $config->get('oecd_publish_needed_field');
      $node->set($oecd_publish_needed_field, TRUE);
    }
  }
}

/**
 * Return whether the node needs to published with the OECD GlobalRecalls API.
 *
 * @param \Drupal\node\NodeInterface $node
 *   The node.
 *
 * @return bool
 *   TRUE if the node needs to be published, FALSE otherwise.
 */
function _psa_oecd_publishing_node_needs_oecd_publish(NodeInterface $node): bool {
  $config = \Drupal::config('psa_oecd_publishing.settings');

  // Allow processes such as migration to enter raw values without any
  // cleverness here.
  if (!empty($node->moderation_state->skip_processing) || !empty($node->moderation_state->bulk_update)) {
    return FALSE;
  }

  // New nodes should be published to OECD if the node is published. In real use
  // $node->original will always be empty for a new node. This extra check makes
  // kernel tests of this function easier.
  if ($node->isNew() && empty($node->original)) {
    return psa_oecd_publishing_node_is_published($node);
  }

  $oecd_publish_needed_field = $config->get('oecd_publish_needed_field');

  // We can't rely on $node->original as drupal will actually give us the
  // default revision even if we are saving a non-default one, so we have to
  // load it out ourselves if it exists.
  $new_state = psa_oecd_publishing_get_moderation_state($node);
  $new_publish_to_oecd = (bool) $node->get($oecd_publish_needed_field)->value;
  $revisionId = $node->getRevisionId();

  if (isset($revisionId)) {
    $storage = \Drupal::entityTypeManager()->getStorage('node');
    $storage->resetCache([$node->id()]);
    /** @var \Drupal\node\NodeInterface $original */
    $original = $storage->loadRevision($revisionId);
    if (!$original) {
      throw new Exception('Missing original revision.');
    }
  }
  else {
    $original = $node->original;
    if (!$original) {
      throw new Exception('Missing $node->original.');
    }
  }
  $original_state = psa_oecd_publishing_get_moderation_state($original);
  $original_publish_to_oecd = (bool) $original->get($oecd_publish_needed_field)->value;

  // Do not update OECD flag if it was just set to FALSE (eg node was just
  // published to OECD).
  if (
    $new_publish_to_oecd === $original_publish_to_oecd
    &&
    (
      ($new_state === 'published')
      ||
      ($new_state != $original_state && $original_state === 'published')
    )
  ) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Return the moderation state of a node.
 *
 * This uses $moderation_state if available, otherwise ::isPublished().
 *
 * @param \Drupal\node\NodeInterface $node
 *   The node.
 *
 * @return string
 *   The moderation state.
 */
function psa_oecd_publishing_get_moderation_state(NodeInterface $node): string {
  return $node->moderation_state->value ?? ($node->isPublished() ? 'published' : 'unpublished');
}

/**
 * Returns whether or not the entity is published.
 *
 * This uses psa_oecd_publishing_get_moderation_state().
 *
 * @param \Drupal\node\NodeInterface $node
 *   The node.
 *
 * @return bool
 *   TRUE if the node is published, FALSE otherwise.
 */
function psa_oecd_publishing_node_is_published(NodeInterface $node): bool {
  return psa_oecd_publishing_get_moderation_state($node) === 'published';
}

/**
 * Implements hook_views_query_alter().
 */
function psa_oecd_publishing_views_query_alter(ViewExecutable $view, QueryPluginBase $query): void {
  $config = \Drupal::config('psa_oecd_publishing.settings');
  $recalls_view_id = $config->get('recalls_view_id');

  // When the recalls View is called from OecdPublishingController::getNodes().
  if ($view->id() === $recalls_view_id && ($view->args['psa_oecd_publishing'] ?? NULL) === 'only-publish-needed') {
    $oecd_publish_needed_field = $config->get('oecd_publish_needed_field');
    // Alter the query so that only nodes that are to be published appear.
    $table = 'node__' . $oecd_publish_needed_field;
    $field = $oecd_publish_needed_field . '_value';
    $table_alias = $query->addTable($table);
    // In testing, it may not be able to add the table and that is fine. This
    // should not happen in non-test usage. If it does, log.
    if ($table_alias) {
      $query->addWhere(0, $table_alias . '.' . $field, 1);
    }
    else {
      \Drupal::logger('psa_oecd_publishing')->critical('Unable to add publish needed field table to query.');
    }
  }
}
