# OECD GlobalRecalls API

The OECD GlobalRecalls portal was created in 2012 to serve as a single window
for sharing information about product recalls world-wide. Use the
psa_oecd_publishing module to publish certain nodes from your Drupal site to the
OECD GlobalRecalls portal.

OECD GlobalRecalls API documentation:

https://globalrecalls-pp.oecd.org/#/admin/import-documentation/

## Configuration

1. Enable psa_oecd_publishing module.
2. Visit admin/config/services/psa-oecd-publishing. Under "Recall content type",
it lists fields that need to exist on your content type for it to work with the
module. Add any missing fields to your content type.
3. Create a View Display which shows all recall nodes which should be published
on the OECD GlobalRecalls portal. Include unpublished nodes; these will be
deleted from the portal. Make the View unpaged unless you want to only act on so
many recalls at a time.
4. Visit admin/config/services/psa-oecd-publishing and do the initial
configuration. After you select your content type and save, the page will allow
you to configure the mapping between the OECD GlobalRecalls fields and the
fields on your Drupal content type. Leave "Use OECD Recalls API production host"
unchecked until you have completed testing. Set "Recall content type" to match
the type used in the View created in step 3.
5. Set module permissions to those who need to use them.

Once testing is complete, check "Use OECD Recalls API production host".

## Usage

Publish recalls directly:

`$drush oecd-publish-recalls`

Publish recalls via queue:

`$drush oecd-publish-recalls --async`

Process queue manually:

`$drush queue:run psa_oecd_publishing_queue`

Clean up old files:

`$drush psa:oecd-clean-up`
