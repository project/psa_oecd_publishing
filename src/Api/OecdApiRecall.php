<?php

namespace Drupal\psa_oecd_publishing\Api;

use Symfony\Component\Serializer\Encoder\XmlEncoder;

/**
 * Class to represent a recall.
 */
class OecdApiRecall {
  use OecdApiTrait;

  /**
   * Array of fields for the XML request.
   *
   * @var array
   */
  public array $request;

  /**
   * Array of files to be included with the request.
   *
   * @var array
   */
  public array $files;

  /**
   * Creates a OECD API Recall instance.
   *
   * @param string $lang
   *   Language of the recall.
   * @param string $jurisdiction
   *   Jurisdiction of the recall.
   * @param string $id
   *   ID of the recall.
   */
  public function __construct(
    protected string $lang,
    protected string $jurisdiction,
    protected string $id,
  ) {
    // Validate the language.
    if (!self::isValidLanguage($lang)) {
      throw new \Exception('Invalid language.');
    }

    // Validate the jurisdiction.
    if (!self::isValidJurisdiction($jurisdiction)) {
      throw new \Exception('Invalid jurisdiction.');
    }

    // Validate the ID.
    if (!$id) {
      throw new \Exception('Invalid recall ID.');
    }
  }

  /**
   * Gets a value from the object.
   *
   * @param string $name
   *   The name of the property to get.
   *
   * @return string
   *   The value of the property.
   */
  public function __get(string $name): string {
    return $this->$name;
  }

  /**
   * Return an XML representation of a recall.
   *
   * @return string|null
   *   The serialized XML or NULL if the $request data has not been set.
   */
  public function getXml(): ?string {
    if (empty($this->request)) {
      return NULL;
    }

    // Set root attributes.
    $elements = [
      '@xmlns:xsd' => 'http://www.w3.org/2001/XMLSchema',
      '@xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
      '@lang' => $this->lang,
      '@xmlns' => 'urn:recall-schema',
    ];
    // Add the request elements.
    $elements += $this->request;

    // Encode as XML.
    $encoder = new XmlEncoder();
    $context = [
      'xml_root_node_name' => 'recall',
      'remove_empty_tags' => TRUE,
      'xml_format_output' => TRUE,
      'xml_encoding' => 'UTF-8',
      'xml_version' => '1.0',
    ];
    $xmlRecall = $encoder->encode($elements, 'xml', $context);
    // Workaround Symfony bug: https://github.com/symfony/symfony/issues/26168.
    // Remove CDATA as all text is encoded.
    $xmlRecall = str_replace('<![CDATA[', '', $xmlRecall);
    $xmlRecall = str_replace(']]>', '', $xmlRecall);
    return $xmlRecall;
  }

}
