<?php

namespace Drupal\psa_oecd_publishing\Api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;

/**
 * Provides interface to send requests to OECD API endpoints.
 */
class OecdApi {

  const OECD_API_URL_POST = 'ws/import.xqy';
  const OECD_API_URL_RECALL_HTML = '#/recalls/';
  const OECD_API_URL_RECALL_JSON = 'ws/getrecall.xqy?uri=';
  const OECD_API_URL_RECALL_URI = 'http://PoliciesApplications.oecd.org/GlobalRecalls/Recall';
  const HTTP_CLIENT_CONFIG = ['timeout' => 120];

  /**
   * Creates a OECD API client instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The configuration factory.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   A Guzzle HTTP client instance.
   */
  public function __construct(
    protected ConfigFactoryInterface $config,
    protected ClientInterface $httpClient,
  ) {
  }

  /**
   * Ping OECD endpoint to confirm API key and connection.
   *
   * @return array
   *   Format: ['isSuccess' => boolean, 'error' => string].
   */
  public function ping(): array {
    $expectedResponse = 'no zip file provided';
    try {
      $request = new Request(
        'GET',
        $this->getPostUrl()->toString(),
      );
      $this->httpClient->send($request, static::HTTP_CLIENT_CONFIG);
    }
    catch (RequestException $exception) {
      $responseBody = $exception->getResponse()?->getBody();
      if ($responseBody && str_contains(strtolower($responseBody), $expectedResponse)) {
        return [
          'isSuccess' => TRUE,
          'error' => '',
        ];
      }
      else {
        return [
          'isSuccess' => FALSE,
          'error' => $exception->getMessage(),
        ];
      }
    }
    catch (\Throwable $exception) {
      return [
        'isSuccess' => FALSE,
        'error' => $exception->getMessage(),
      ];
    }

    return [
      'isSuccess' => TRUE,
      'error' => '',
    ];
  }

  /**
   * Return the URL of the OECD import API endpoint to use.
   *
   * @return string
   *   The URL.
   */
  private function getApiUrl(): string {
    $config = $this->config->get('psa_oecd_publishing.settings');

    $host = $config->get('oecd_api_use_production_host') ?
      $config->get('oecd_api_host_production') :
      $config->get('oecd_api_host_testing');

    return 'https://' . $host . '/';
  }

  /**
   * Return the URL of the OECD import API endpoint with API key.
   *
   * @return Drupal\Core\Url
   *   The URL of the OECD import API endpoint with API key.
   */
  public function getPostUrl(): Url {
    $oecd_config = $this->config->get('psa_oecd_publishing.settings');
    $oecd_import_url = $this->getApiUrl() . self::OECD_API_URL_POST;
    $oecd_api_key = $oecd_config->get('oecd_api_key');

    $options = [
      'query' => [
        'apikey' => $oecd_api_key,
      ],
    ];
    return Url::fromUri($oecd_import_url, $options);
  }

  /**
   * Return the URI of a recall.
   *
   * @param Drupal\psa_oecd_publishing\Api\OecdApiRecall $recall
   *   The recall.
   *
   * @return string
   *   The URI of the recall.
   */
  private static function getRecallUri(OecdApiRecall $recall): string {
    $uri_pieces = [
      self::OECD_API_URL_RECALL_URI,
      strtoupper($recall->lang),
      $recall->jurisdiction,
      rawurlencode($recall->id),
    ];
    return implode('/', $uri_pieces);
  }

  /**
   * Return the URL of the JSON or HTML representation of a recall.
   *
   * @param Drupal\psa_oecd_publishing\Api\OecdApiRecall $recall
   *   The recall.
   * @param bool $html
   *   Then TRUE, return the URL of the HTML representation, otherwise, JSON.
   *
   * @return Drupal\Core\Url
   *   The URL of the recall.
   */
  public function getRecallUrl(OecdApiRecall $recall, bool $html = FALSE): Url {
    $path = $html ? self::OECD_API_URL_RECALL_HTML : self::OECD_API_URL_RECALL_JSON;
    $url = $this->getApiUrl() . $path . rawurlencode(static::getRecallUri($recall));
    return Url::fromUri($url);
  }

  /**
   * GET recall details from OECD by ID.
   *
   * @param Drupal\psa_oecd_publishing\Api\OecdApiRecall $recall
   *   The recall.
   *
   * @return mixed
   *   Contents of a recall or NULL if the recall ID does not exist.
   *
   * @throws \Throwable
   */
  public function get(OecdApiRecall $recall): mixed {
    $headers = [];
    $httpBody = '';

    try {
      $request = new Request(
        'GET',
        $this->getRecallUrl($recall)->toString(),
        $headers,
        $httpBody
      );
      $response = $this->httpClient->send($request, static::HTTP_CLIENT_CONFIG);
    }
    catch (RequestException $exception) {
      if ($exception->getCode() === 404) {
        return NULL;
      }
      throw $exception;
    }

    $statusCode = $response->getStatusCode();

    if ($statusCode >= 200 && $statusCode <= 299) {
      $responseBody = $response->getBody();
      try {
        $content = $responseBody->getContents();
        $content = json_decode($content);
      }
      catch (\Throwable $exception) {
        $content = NULL;
      }
      return $content;
    }
    else {
      throw new \Exception(
        sprintf(
          '[%d] Error connecting to the API (%s)',
          $statusCode,
          $request->getUri()
        )
      );
    }
  }

  /**
   * Send a POST request to OECD with recall archive as contents.
   *
   * @param string $filePath
   *   Path to file to be POSTed to OECD.
   *
   * @return bool
   *   TRUE if request was successful, FALSE otherwise.
   *
   * @throws \Throwable
   */
  public function post(string $filePath): bool {
    $headers = [
      'Content-Type' => 'application/zip',
    ];

    try {
      $httpBody = fopen($filePath, 'r');

      $request = new Request(
        'POST',
        $this->getPostUrl()->toString(),
        $headers,
        $httpBody
      );
      $response = $this->httpClient->send($request, static::HTTP_CLIENT_CONFIG);
      $statusCode = $response->getStatusCode();
    }
    catch (\Throwable $exception) {
      throw $exception;
    }

    return $statusCode === 200;
  }

}
