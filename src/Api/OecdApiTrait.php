<?php

namespace Drupal\psa_oecd_publishing\Api;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Support for OECD API.
 */
trait OecdApiTrait {

  /**
   * The OECD API allows only English and French.
   *
   * @var array
   */
  public static array $supportedLanguages = [
    'en',
    'fr',
  ];

  /**
   * Returns whether a value is a jurisdiction code accepted by the OECD API.
   *
   * This accepts any two-letter code; it does not validate if it is a valid
   * code in ISO 3166-1 alpha 2.
   *
   * @param string $jurisdiction
   *   The code to check.
   *
   * @return bool
   *   TRUE if the code is valid, FALSE otherwise.
   *
   * @see https://globalrecalls-pp.oecd.org/#/admin/import-documentation/?section=fields-description.country-id
   */
  public static function isValidJurisdiction(string $jurisdiction): bool {
    // Array of valid values allowed by OECD API.
    $valid = [
      'AE-AZ',
      'AE-AJ',
      'AE-FU',
      'AE-SH',
      'AE-DU',
      'AE-RK',
      'AE-UQ',
    ];
    // Return TRUE if the value matches the pattern or is listed as valid.
    return preg_match('/^[A-Z]{2}$/', $jurisdiction) || in_array($jurisdiction, $valid, TRUE);
  }

  /**
   * Returns whether a value is a language code accepted by the OECD API.
   *
   * @param string $lang
   *   The code to check.
   *
   * @return bool
   *   TRUE if the code is valid, FALSE otherwise.
   *
   * @see https://globalrecalls-pp.oecd.org/#/admin/import-documentation/?section=fields-description.recall
   */
  public static function isValidLanguage(string $lang): bool {
    return in_array($lang, static::$supportedLanguages, TRUE);
  }

  /**
   * Returns the valid language codes an entity exists in.
   *
   * Only languages supported by the OECD GlobalRecalls API are returned.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity to read the languages from.
   *
   * @return string[]
   *   An array of language codes.
   */
  public static function getRecallLanguages(ContentEntityInterface $entity): array {
    $translations = [];

    foreach ($entity->getTranslationLanguages() as $langcode => $language) {
      // Skip languages that are not supported by the OECD GlobalRecalls API.
      if (!in_array($langcode, static::$supportedLanguages, TRUE)) {
        continue;
      }

      $translations[] = $langcode;
    }

    return $translations;
  }

}
