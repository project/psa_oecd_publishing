<?php

namespace Drupal\psa_oecd_publishing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\node\NodeInterface;
use Drupal\psa_oecd_publishing\Api\OecdApi;
use Drupal\psa_oecd_publishing\Api\OecdApiRecall;
use Drupal\psa_oecd_publishing\Api\OecdApiTrait;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller to communicate with OECD Publishing.
 *
 * @todo If a node is edited, but the edits do not change any fields sent to the
 * OECD GlobalRecalls API, an update, which will do nothing, will still be sent.
 * It would be better to check and not send an update in this case.
 */
class OecdPublishingController extends ControllerBase {
  use OecdApiTrait;

  /**
   * Creates a OecdPublishingController instance.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file_system service.
   * @param \GuzzleHttp\Client $httpClient
   *   The http_client service.
   * @param \Drupal\psa_oecd_publishing\Api\OecdApi $oecdApi
   *   The oecd_api.recall service.
   * @param OecdMapperController $mapper
   *   The psa_oecd_mapper.controller service.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue service.
   */
  public function __construct(
    protected FileSystemInterface $fileSystem,
    protected Client $httpClient,
    public OecdApi $oecdApi,
    public OecdMapperController $mapper,
    protected QueueFactory $queueFactory,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): object {
    return new static(
      $container->get('file_system'),
      $container->get('http_client'),
      $container->get('oecd_api.recall'),
      $container->get('psa_oecd_mapper.controller'),
      $container->get('queue'),
    );
  }

  /**
   * Publish recalls to OECD.
   *
   * @param bool $useQueue
   *   Flag indicating if request upload should be handled by the queue process.
   * @param array|null $nodes
   *   If an array, publish this set of nodes. Otherwise, use ::getNodes().
   *
   * @return array
   *   An unkeyed array.
   *   The first return parameter is a boolean status (TRUE if publishing
   *   process completed, otherwise FALSE).
   *   The second return parameter is a keyed array containing total, passed and
   *   failed counts.
   */
  public function publish(bool $useQueue = FALSE, array $nodes = NULL): array {
    $config = $this->config('psa_oecd_publishing.settings');
    $path = $config->get('oecd_export_path');
    $isReady = $this->fileSystem->prepareDirectory(
      $path,
      FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
    );

    if (!$isReady) {
      $this->getLogger('psa_oecd_publishing')->error('Failed to prepare directory for @path.', ['@path' => $path]);
      return [FALSE, []];
    }

    $isReady = $this->oecdApi->ping();
    if (!$isReady['isSuccess']) {
      $this->getLogger('psa_oecd_publishing')->error('Failed to connect to API endpoint. @error', ['@error' => $isReady['error']]);
      return [FALSE, []];
    }

    // Use $nodes if passed into function.
    $nodes ??= $this->getNodes();
    $nodeCount = count($nodes);
    if ($nodeCount === 0) {
      $this->getLogger('psa_oecd_publishing')->notice('There are no updates to publish to OECD.');
      return [FALSE, []];
    }

    $failedCount = $this->processNodes($nodes, $useQueue);

    $this->getLogger('psa_oecd_publishing')->notice(
      'Publishing to OECD completed. Nodes processed: @total. Succeeded: @passed. Failed: @failed.',
      [
        '@total' => $nodeCount,
        '@passed' => $nodeCount - $failedCount,
        '@failed' => $failedCount,
      ]
    );

    $status = [
      'total' => $nodeCount,
      'passed' => $nodeCount - $failedCount,
      'failed' => $failedCount,
    ];
    return [TRUE, $status];
  }

  /**
   * Processes nodes for OECD publishing.
   *
   * @param array $nodes
   *   Array of recall nodes to publish.
   * @param bool $useQueue
   *   Flag indicating whether to use queue processing or not.
   *
   * @return int
   *   The number of nodes that failed to process.
   */
  private function processNodes(array $nodes, bool $useQueue = FALSE): int {
    $failedCount = 0;

    foreach ($nodes as $node) {
      $nodeId = $node->id();
      $archiveFileNames = [];

      $isSuccess = FALSE;

      foreach (static::getRecallLanguages($node) as $langcode) {
        $context = [
          '@nodeId' => $nodeId,
          '@langcode' => $langcode,
        ];

        $archiveFileName = $this->processNode($node->getTranslation($langcode));

        if (!isset($archiveFileName)) {
          $this->getLogger('psa_oecd_publishing')->error('Failed to process node @nodeId in language @langcode.', $context);
          $isSuccess = FALSE;
        }
        elseif ($useQueue) {
          $archiveFileNames[$langcode] = $archiveFileName;
          $isSuccess = TRUE;
        }
        else {
          $isSuccess = $this->postRecall($archiveFileName, $nodeId, $langcode);
          if (!$isSuccess) {
            $this->getLogger('psa_oecd_publishing')->error('Failed to publish node @nodeId in language @langcode.', $context);
          }
        }

        // On any failure, do not try any more languages.
        if (!$isSuccess) {
          break;
        }
      }

      if ($isSuccess) {
        if ($useQueue) {
          $isSuccess = $this->postRecallViaQueue($archiveFileNames, $nodeId);
          if (!$isSuccess) {
            $this->getLogger('psa_oecd_publishing')->error('Failed to queue node @nodeId.', ['@nodeId' => $nodeId]);
          }
        }
        else {
          // For all languages, postRecall() succeeded.
          $this->updateNodeStatus($node);
        }
      }

      if (!$isSuccess) {
        $failedCount++;
      }
    }

    return $failedCount;
  }

  /**
   * Flag that this node has been updated in the OECD GlobalRecalls portal.
   *
   * This runs when a recall node is updated in the OECD GlobalRecalls portal,
   * un-setting the flag that says it needs to be updated. When a recall node is
   * edited, the flag is set in psa_oecd_publishing_node_presave().
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $node
   *   Recall node.
   * @param bool $statusOnly
   *   Update status only, not time stamp.
   */
  public function updateNodeStatus(FieldableEntityInterface $node, bool $statusOnly = FALSE): void {
    $config = $this->config('psa_oecd_publishing.settings');
    $oecd_publish_needed_field = $config->get('oecd_publish_needed_field');
    $oecd_publish_timestamp_field = $config->get('oecd_publish_timestamp_field');

    try {
      $node->set($oecd_publish_needed_field, FALSE);
      if (!$statusOnly) {
        $node->set($oecd_publish_timestamp_field, time());
      }
      $node->save();
    }
    catch (\Throwable $exception) {
      $this->getLogger('psa_oecd_publishing')->error($exception->getMessage());
    }
  }

  /**
   * Check if recall exists on OECD.
   *
   * @param Drupal\psa_oecd_publishing\Api\OecdApiRecall $recall_object
   *   The recall.
   *
   * @return bool
   *   TRUE if recall exists, FALSE otherwise.
   */
  public function recallExists(OecdApiRecall $recall_object): bool {
    $recall = NULL;
    try {
      $recall = $this->oecdApi->get($recall_object);
    }
    catch (\Throwable $exception) {
      $this->getLogger('psa_oecd_publishing')->warning($exception->getMessage());
    }

    return isset($recall);
  }

  /**
   * Upload recall request file to OECD.
   *
   * @param string $path
   *   Path to file to be uploaded in request.
   * @param int $nodeId
   *   Node id of the corresponding request.
   * @param string $langcode
   *   Node language code of the recall.
   *
   * @return bool
   *   TRUE if upload was successful, FALSE otherwise.
   */
  public function postRecall(string $path, int $nodeId, string $langcode): bool {
    $response = FALSE;
    try {
      $response = $this->oecdApi->post($path);
      $context = [
        '@nodeId' => $nodeId,
        '@langcode' => $langcode,
      ];
      $this->getLogger('psa_oecd_publishing')->notice('Successfully processed node @nodeId in language @langcode.', $context);
    }
    catch (\Throwable $exception) {
      $this->getLogger('psa_oecd_publishing')->error($exception->getMessage());
    }
    return $response;
  }

  /**
   * Clean up OECD recall files/folders.
   *
   * @param bool $all
   *   If set to TRUE all files will be deleted, if set to FALSE ONLY files
   *   older than 2 days.
   *
   * @return bool
   *   TRUE if cleanup executed successfully.
   */
  public function cleanup(bool $all = FALSE): bool {
    $config = $this->config('psa_oecd_publishing.settings');
    $path = $config->get('oecd_export_path');
    $isReady = $this->fileSystem->prepareDirectory(
      $path,
      FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
    );

    if (!$isReady) {
      $this->getLogger('psa_oecd_publishing')->error('Failed to prepare directory for @path.', ['@path' => $path]);
      return FALSE;
    }

    try {
      $items = $this->fileSystem->scanDirectory($path, '/.+/', ['recurse' => FALSE]);
      $twoDays = time() - 2 * 24 * 60 * 60;
      $count = 0;
      foreach ($items as $key => $file) {
        if ($all || filemtime($key) < $twoDays) {
          $this->fileSystem->deleteRecursive($key);
          $count++;
        }
      }
    }
    catch (\Throwable $exception) {
      $context = [
        '@path' => $path,
        '@message' => $exception->getMessage(),
      ];
      $this->getLogger('psa_oecd_publishing')->error('Failed to clean up directory for @path. Exception message: @message', $context);
      return FALSE;
    }

    if ($count > 0) {
      $this->getLogger('psa_oecd_publishing')->notice('Cleaned up $count files in @path.', ['@path' => $path]);
    }
    else {
      $this->getLogger('psa_oecd_publishing')->notice('There are no files to clean up.');
    }

    return TRUE;
  }

  /**
   * Post recall to the queue for processing.
   *
   * @param array $archiveNames
   *   An array of paths to files to be uploaded in request keyed by langcode.
   * @param int $nodeId
   *   Node ID of the recall.
   *
   * @return bool
   *   TRUE if posting on the queue was successful.
   */
  private function postRecallViaQueue(array $archiveNames, int $nodeId): bool {
    $config = $this->config('psa_oecd_publishing.settings');
    $isSuccess = FALSE;
    try {
      $this->getLogger('psa_oecd_publishing')->notice('Publish request for node @nodeId has been sent to the queue.', ['@nodeId' => $nodeId]);
      $queueName = $config->get('queue_name');
      $queue = $this->queueFactory->get($queueName);
      $queue->createQueue();
      $queue->createItem([
        'node_id' => $nodeId,
        'archive_file_names' => $archiveNames,
      ]);
      $isSuccess = TRUE;
    }
    catch (\Throwable $exception) {
      $this->getLogger('psa_oecd_publishing')->error($exception->getMessage());
    }
    return $isSuccess;
  }

  /**
   * Return nodes that are to be published to the OECD GlobalRecalls API.
   *
   * @return \Drupal\node\NodeInterface[]
   *   Array of nodes keyed by nid.
   */
  public function getNodes(): array {
    $config = $this->config('psa_oecd_publishing.settings');
    $viewId = $config->get('recalls_view_id');
    $displayId = $config->get('recalls_view_display_id');
    $oecd_publish_needed_field = $config->get('oecd_publish_needed_field');
    $recall_data_content_type = $config->get('recall_data_content_type');

    // Raise error if any config is missing.
    if (!$viewId || !$displayId || !$oecd_publish_needed_field || !$recall_data_content_type) {
      $this->getLogger('psa_oecd_publishing')->warning('Module is not configured.');
      return [];
    }

    try {
      $view = $this->entityTypeManager()->getStorage('view')->load($viewId);
      $display = $view->getDisplay($displayId);
      if (empty($display)) {
        $context = [
          '@viewId' => $viewId,
          '@displayId' => $displayId,
        ];
        $this->getLogger('psa_oecd_publishing')->warning('Failed to execute view @viewId with display ID @displayId. Check view display id exists.', $context);
        return [];
      }
      $view = $view->getExecutable();
    }
    catch (\Throwable $exception) {
      $this->getLogger('psa_oecd_publishing')->warning('Failed to execute view @viewId. Check view exists.', ['@viewId' => $viewId]);
      return [];
    }

    // Alter the View so that we only get nodes for which the
    // oecd_publish_needed_field is TRUE. Set this argument to trigger altering
    // the View in psa_oecd_publishing_views_query_alter().
    $view->args['psa_oecd_publishing'] = 'only-publish-needed';

    $view->execute($displayId);
    $nodes = [];
    if (isset($view->result) && count($view->result) > 0) {
      $rows = $view->result;
      foreach ($rows as $row) {
        // Include only rows that are nodes of the recall_data_content_type.
        $is_recall_content_type = ($row->_entity instanceof NodeInterface) && $row->_entity->bundle() === $recall_data_content_type;
        if (!$is_recall_content_type) {
          continue;
        }

        // Error if the publish-needed field is missing.
        if (!$row->_entity->hasField($oecd_publish_needed_field)) {
          $this->getLogger('psa_oecd_publishing')->warning('Recall content type is missing publish-needed field.');
          return [];
        }

        // Skip recalls that have a blank recall ID.
        if (!$this->getRecallId($row->_entity)) {
          $this->getLogger('psa_oecd_publishing')->warning('Skipping recall that has a blank recall ID; node: @nid.', ['@nid' => $row->_entity->id()]);
          continue;
        }

        // If this is not keyed by nid, nodes appear more than once.
        // @todo Find out why and fix.
        $nodes[$row->_entity->id()] = $row->_entity;
      }
    }

    return $nodes;
  }

  /**
   * Return the recall ID of the entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Content of the recall.
   *
   * @return string|null
   *   The recall ID or NULL if there is not one.
   */
  public function getRecallId(FieldableEntityInterface $entity): ?string {
    $config = $this->config('psa_oecd_publishing.settings');
    $mapping = $config->get('oecd_recall_field_mapping');

    $recall_id_field = $mapping['id'] ?? NULL;
    if (!$recall_id_field) {
      $this->getLogger('psa_oecd_publishing')->warning('Recall ID field is not configured.');
      return NULL;
    }

    return $this->mapper->getValue($entity, $recall_id_field, FALSE, FALSE);
  }

  /**
   * Return a OecdApiRecall based on a recall node.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Content of the recall.
   *
   * @return \Drupal\psa_oecd_publishing\Api\OecdApiRecall|null
   *   The OecdApiRecall or NULL if the node does not contain valid values.
   */
  public function getRecall(FieldableEntityInterface $entity): ?OecdApiRecall {
    $config = $this->config('psa_oecd_publishing.settings');

    $lang = $entity->language()->getId();
    $jurisdiction = $config->get('recall_jurisdiction');
    $id = $this->getRecallId($entity);

    if (self::isValidLanguage($lang) && self::isValidJurisdiction($jurisdiction) && $id) {
      return new OecdApiRecall($lang, $jurisdiction, $id);
    }
    return NULL;
  }

  /**
   * Return the operation that should be done on this recall.
   *
   * An unpublished recall is one that should not exist in the OECD
   * GlobalRecalls portal.
   *
   * @param \Drupal\psa_oecd_publishing\Api\OecdApiRecall $recall
   *   The recall.
   * @param bool $published
   *   Whether the recall should exist in the OECD GlobalRecalls portal.
   *
   * @return string|null
   *   The operation, one of OECD_OPERATION_INSERT, OECD_OPERATION_UPDATE, or
   *   OECD_OPERATION_DELETE; NULL if no operation should be performed.
   */
  public function getRecallOperation(OecdApiRecall $recall, bool $published): ?string {
    $recallExists = $this->recallExists($recall);

    // If the recall is published, do an insert or update, depending on whether
    // it exists in the OECD GlobalRecalls portal.
    if ($published) {
      return $recallExists ?
        OecdMapperController::OECD_OPERATION_UPDATE :
        OecdMapperController::OECD_OPERATION_INSERT;
    }
    // If the node is unpublished, delete it if it exists in the OECD
    // GlobalRecalls portal.
    elseif ($recallExists) {
      return OecdMapperController::OECD_OPERATION_DELETE;
    }
    // If recall is unpublished and it doesn't exist on OECD, no action needed.
    else {
      return NULL;
    }
  }

  /**
   * Processes node for OECD publishing.
   *
   * Following steps are included:
   *    - Set edit type: insert/update/delete;
   *    - Create XML request;
   *    - Save XML request and images;
   *    - Archive XML request and images.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $node
   *   Content of the recall.
   *
   * @return string|null
   *   Name of archive with request and images or NULL.
   */
  private function processNode(FieldableEntityInterface $node): ?string {
    $config = $this->config('psa_oecd_publishing.settings');
    $nodeId = $node->id();

    $jurisdiction = $config->get('recall_jurisdiction');

    $recallId = $this->getRecallId($node);
    if (!isset($recallId)) {
      $this->getLogger('psa_oecd_publishing')->warning('Recall ID is not set on recall node @nodeId. Skipping recall.', ['@nodeId' => $nodeId]);
      $this->updateNodeStatus($node, TRUE);
      return NULL;
    }

    $recall = new OecdApiRecall($node->language()->getId(), $jurisdiction, $recallId);

    // Get the action that should be performed, returning early if none needed.
    $action = $this->getRecallOperation($recall, $node->status->value == 1);
    if (!$action) {
      $this->getLogger('psa_oecd_publishing')->notice('Unpublished node @nodeId does not exists on OECD. Skipping node.', ['@nodeId' => $nodeId]);
      $this->updateNodeStatus($node, TRUE);
      return NULL;
    }

    // Get XML request from node's data.
    $this->mapper->getRequest(
      $action,
      $recall,
      $node,
      $config->get('oecd_recall_field_mapping')
    );

    return $this->buildRequestArchive($nodeId, $recall->lang, $recall->getXml(), $recall->files);
  }

  /**
   * Create the request archive path with the correct permissions.
   *
   * @param string $nodeId
   *   The node ID.
   * @param string $langcode
   *   The language code.
   *
   * @return string|null
   *   The path or NULL if creation failed.
   */
  public function createRequestArchivePath(string $nodeId, string $langcode): ?string {
    $config = $this->config('psa_oecd_publishing.settings');
    $path = $config->get('oecd_export_path');
    $nodePath = $path . '/' . time() . '-' . $nodeId . '-' . $langcode;

    $isReady = $this->fileSystem->prepareDirectory(
      $nodePath,
      FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
    );
    if (!$isReady) {
      $this->getLogger('psa_oecd_publishing')->error('Failed to prepare directory for @nodePath.', ['@nodePath' => $nodePath]);
      return NULL;
    }

    return $nodePath;
  }

  /**
   * Creates directories and builds contents for ZIP archive.
   *
   * @param string $nodeId
   *   The node ID.
   * @param string $langcode
   *   The language code.
   * @param string $xmlRequest
   *   The XML of the request.
   * @param array $fileNames
   *   An array of filenames of files to include with the request.
   *
   * @return string|null
   *   Name of archive with request and images or NULL.
   */
  public function buildRequestArchive(string $nodeId, string $langcode, string $xmlRequest, array $fileNames): ?string {
    $nodePath = $this->createRequestArchivePath($nodeId, $langcode);
    if (!$nodePath) {
      return NULL;
    }

    // Save XML request as XML file.
    $xmlFileName = $nodeId . '-' . $langcode . '.xml';
    try {
      $this->fileSystem->saveData(
        $xmlRequest,
        "$nodePath/$xmlFileName",
        FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::EXISTS_REPLACE
      );
    }
    catch (\Throwable $exception) {
      $context = [
        '@nodeId' => $nodeId,
        '@message' => $exception->getMessage(),
      ];
      $this->getLogger('psa_oecd_publishing')->error('Error processing node @nodeId: @message', $context);
      return NULL;
    }

    $fileNames[] = [
      'path' => "$nodePath/$xmlFileName",
      'name' => $xmlFileName,
    ];

    // Add all files to archive.
    $archiveFileName = $nodeId . '-' . $langcode . '.zip';
    try {
      $this->createRequestArchive($nodePath, $archiveFileName, $fileNames);
    }
    catch (\Throwable $exception) {
      $context = [
        '@nodeId' => $nodeId,
        '@message' => $exception->getMessage(),
      ];
      $this->getLogger('psa_oecd_publishing')->error('Error processing node @nodeId: @message', $context);
      return NULL;
    }

    // Return archive file location.
    return "$nodePath/$archiveFileName";
  }

  /**
   * Creates ZIP archive.
   *
   * @param string $path
   *   Path to files to be archived.
   * @param string $archiveFileName
   *   Name of the archive.
   * @param array $filesToAdd
   *   Array of file names to be added to the archive.
   *
   * @return bool
   *   TRUE if archive was created.
   *
   * @throws \Throwable
   */
  private function createRequestArchive(string $path, string $archiveFileName, array $filesToAdd): bool {
    $zip = new \ZipArchive();
    $isOpen = $zip->open(
      $this->fileSystem->realpath("$path/$archiveFileName"),
      \ZipArchive::CREATE
    );

    if ($isOpen !== TRUE) {
      throw new \Exception("Could not open archive $path/$archiveFileName. Error code: $isOpen");
    }

    foreach ($filesToAdd as $fileToAdd) {
      $realPath = $this->fileSystem->realpath($fileToAdd['path']);
      if (!$realPath) {
        throw new \Exception(sprintf('File missing: %s/%s.', $path, $fileToAdd['name']));
      }
      try {
        $isSuccess = $zip->addFile(
          $realPath,
          $fileToAdd['name']
        );
      }
      catch (\Throwable $exception) {
        throw new \Exception(sprintf('Could not add file %s/%s to the archive: %s', $path, $fileToAdd['name'], $exception->getMessage()));
      }
      if (!$isSuccess) {
        throw new \Exception(sprintf('Could not add file %s/%s to the archive.', $path, $fileToAdd['name']));
      }
    }

    $isSuccess = $zip->close();
    if (!$isSuccess) {
      throw new \Exception("Could not close archive $path/$archiveFileName.");
    }

    return $isSuccess;
  }

}
