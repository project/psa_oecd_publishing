<?php

namespace Drupal\psa_oecd_publishing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\psa_oecd_publishing\Api\OecdApiRecall;
use Drupal\psa_oecd_publishing\Api\OecdApiTrait;

/**
 * Maps node values into OECD XML request.
 */
class OecdMapperController extends ControllerBase {

  use OecdApiTrait;

  const OECD_OPERATION_INSERT = 'I';
  const OECD_OPERATION_UPDATE = 'U';
  const OECD_OPERATION_DELETE = 'D';
  const OECD_OPERATIONS = [
    self::OECD_OPERATION_INSERT,
    self::OECD_OPERATION_UPDATE,
    self::OECD_OPERATION_DELETE,
  ];

  /**
   * Returns the name of an OECD API operation.
   *
   * @param string $operation
   *   One of the values of self::OECD_OPERATIONS.
   *
   * @return string
   *   The translated full name of the operation.
   */
  public static function getOperationName(string $operation): string {
    switch ($operation) {
      case self::OECD_OPERATION_INSERT:
        return t('Insert');

      case self::OECD_OPERATION_UPDATE:
        return t('Update');

      case self::OECD_OPERATION_DELETE:
        return t('Delete');
    }
    throw new \Exception('Invalid operation.');
  }

  /**
   * Returns information about the fields of an OECD GlobalRecalls API request.
   *
   * Keys are in the order they must appear in the request.
   *
   * @return array[]
   *   Array keyed by OECD Recall API field keys. The values are an array with
   *   the following keys:
   *    - include_for_delete: bool; TRUE for fields that are included in delete
   *      requests, FALSE otherwise.
   *    - mappable: bool; TRUE for fields that can be mapped by the user; FALSE
   *      for fields that are not supported or determined programmatically.
   *    - multiple: bool; TRUE for fields that can have multiple values.
   *    - required: bool; TRUE for fields defined as mandatory in the OECD
   *      Recalls API.
   *    - type: string; The data type for this field.
   */
  public static function getFields(): array {
    $fields = [
      // Not mappable: Determined programmatically.
      'edit_type' => [
        'include_for_delete' => TRUE,
        'mappable' => FALSE,
        'required' => TRUE,
      ],
      'id' => ['include_for_delete' => TRUE, 'required' => TRUE],
      // Not mappable: From config.
      'country_id' => [
        'include_for_delete' => TRUE,
        'mappable' => FALSE,
        'required' => TRUE,
      ],
      'manufacturer' => [],
      'manufacturer_country_id' => [],
      'product_type' => ['multiple' => TRUE],
      'product_name' => ['required' => TRUE],
      'product_desc' => [],
      // Not mappable: Not supported. @todo Implement support for this type.
      'product_code' => ['mappable' => FALSE, 'type' => 'product_code'],
      'product_model_no' => [],
      'hazard' => [],
      'injuries' => [],
      'action' => [],
      'units' => [],
      // Not mappable: URL of the node determined programmatically.
      'URL' => ['mappable' => FALSE],
      'date' => ['required' => TRUE, 'type' => 'date'],
      'distributor_name' => [],
      'distributor_website' => [],
      'GTIN' => ['type' => 'gtin'],
      'GTIN_product_type' => [],
      'GTIN_desc' => [],
      'HTS' => [],
      'HTS_product_type' => [],
      'HTS_desc' => [],
      'image' => ['type' => 'images'],
      'manufacturer_website' => [],
      'serious_risk' => ['type' => 'bool'],
    ];
    // Set default values.
    foreach ($fields as $key => $info) {
      $fields[$key]['include_for_delete'] = $fields[$key]['include_for_delete'] ?? FALSE;
      $fields[$key]['mappable'] = $fields[$key]['mappable'] ?? TRUE;
      $fields[$key]['multiple'] = $fields[$key]['multiple'] ?? FALSE;
      $fields[$key]['required'] = $fields[$key]['required'] ?? FALSE;
      $fields[$key]['type'] = $fields[$key]['type'] ?? 'string';
    }
    return $fields;
  }

  /**
   * Creates XML Request for the provided content.
   *
   * @param string $editType
   *   Type of the request Insert/Update/Delete.
   * @param \Drupal\psa_oecd_publishing\Api\OecdApiRecall $oecdApiRecall
   *   The recall object to add the OECD GlobalRecalls API fields to.
   * @param \Drupal\Core\Entity\FieldableEntityInterface|null $entity
   *   Content of the recall.
   * @param array $oecd_recall_field_mapping
   *   An array in the format:
   *   [oecd_recall_api_field_key => recall_data_field_key].
   */
  public function getRequest(string $editType, OecdApiRecall $oecdApiRecall, FieldableEntityInterface $entity = NULL, array $oecd_recall_field_mapping = []): void {
    // Validate the editType.
    if (!in_array($editType, self::OECD_OPERATIONS, TRUE)) {
      throw new \Exception('Invalid edit type.');
    }

    // Check there is an $entity unless this is a delete.
    if (!$entity && $editType !== self::OECD_OPERATION_DELETE) {
      throw new \Exception('$entity must be provided unless this is a delete request.');
    }

    $recall = [];
    $files = [];

    foreach ($this->getFields() as $oecd_field_key => $oecd_field_info) {
      // During deletes, skip most fields.
      if ($editType === self::OECD_OPERATION_DELETE && !$oecd_field_info['include_for_delete']) {
        continue;
      }
      // The value to set for this $oecd_field_key.
      $value = NULL;

      // Some OECD GlobalRecalls API fields get special handling.
      switch ($oecd_field_key) {
        case 'edit_type':
          $value = $editType;
          break;

        case 'country_id':
          $value = $oecdApiRecall->jurisdiction;
          break;

        case 'id':
          $value = htmlspecialchars($oecdApiRecall->id, ENT_QUOTES | ENT_XML1, 'UTF-8');
          break;

        case 'URL':
          // Generate the URL of this recall.
          $options = ['absolute' => TRUE];
          $value = $entity->toUrl('canonical', $options)->toString();
          break;

        default:
          // The remaining field values are taken from the mapping config. Get
          // the content type field key for the OECD GlobalRecalls API field.
          $recall_data_field_key = $oecd_recall_field_mapping[$oecd_field_key] ?? NULL;
          // Only process fields that are mapped.
          if ($recall_data_field_key) {
            switch ($oecd_field_info['type']) {
              case 'date':
                $value = $entity->get($recall_data_field_key)->date->format('Y-m-d');
                break;

              case 'gtin':
                $value = $this->getValue($entity, $recall_data_field_key);
                // @todo Log when this regex fails. Otherwise, the GTIN will
                // silently not be set on recalls that include an invalid one.
                $value = preg_match('/^[0-9]{8,14}$/', $value) ? $value : NULL;
                break;

              case 'images':
                $images = $this->getImagesFields($entity, $recall_data_field_key);
                $files = $images['files'] ?? [];
                $value = $images['xml'] ?? NULL;
                break;

              case 'bool':
                $value = $this->getValue($entity, $recall_data_field_key) ? 'true' : 'false';
                break;

              case 'product_code':
                // This complex type is not supported by this module.
                $value = NULL;
                break;

              default:
                $value = $this->getValue($entity, $recall_data_field_key, $oecd_field_info['multiple']);
            }
          }
      }

      // Include the value in the recall if it exists.
      if (isset($value)) {
        $recall[$oecd_field_key] = $value;
      }
      // Throw if a required field has been left blank. This should not happen
      // since field mapping configuration requires these to be configured.
      elseif ($oecd_field_info['required']) {
        throw new \Exception('Required field missing: ' . $oecd_field_key);
      }
    }

    $oecdApiRecall->request = $recall;
    $oecdApiRecall->files = $files;

    // Create hook_psa_oecd_publishing_request_alter().
    // Allow modules to alter $recall.
    $this->moduleHandler()->alter('psa_oecd_publishing_request', $oecdApiRecall, $entity);
  }

  /**
   * Get the value of a field from an entity.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   The entity to draw values from; the content of the recall.
   * @param string $fieldName
   *   The name of the field in the entity to get the value from.
   * @param bool $multiple
   *   When TRUE, return all values separated by " ; ", otherwise, only first.
   * @param bool $encode
   *   When TRUE, encode the value with htmlspecialchars().
   *
   * @return string|null
   *   The value or NULL if the field is empty.
   */
  public function getValue(FieldableEntityInterface $entity, string $fieldName, bool $multiple = FALSE, bool $encode = TRUE): ?string {
    switch ($fieldName) {
      case 'id':
        $value = $entity->id();
        break;

      case 'title':
        $value = $entity->getTitle();
        break;

      default:
        $values = $entity->get($fieldName)->getValue();
        if (!isset($values) || count($values) === 0) {
          return NULL;
        }

        // Will be NULL if translation is not enabled.
        $langcode = $entity->language()?->getId();

        // Gather all values for the field.
        $all_values = [];
        foreach ($values as $value) {
          if (isset($value['value'])) {
            $value = $value['value'];
          }
          elseif (isset($value['uri'])) {
            $value = $value['uri'];
          }
          elseif (isset($value['target_id'])) {
            try {
              $termId = $value['target_id'];
              $parents = $this->entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($termId);
            }
            catch (\Throwable $exception) {
              return NULL;
            }

            $labels = [];
            foreach ($parents as $parentTerm) {
              // Get the translated taxonomy term label, if available.
              if ($langcode && $parentTerm->hasTranslation($langcode)) {
                $label = $parentTerm->getTranslation($langcode)->label();
              }
              else {
                $label = $parentTerm->label();
              }

              array_unshift($labels, $label);
            }

            $value = implode(', ', $labels);
          }

          // Only include values that are not the empty string.
          if ((string) $value !== '') {
            $all_values[] = $value;
          }

          // Return only first value if $multiple is not set.
          if (!$multiple) {
            break;
          }
        }
        // Combined multiple values with " ; "; set to NULL if none.
        $value = $all_values ? implode(' ; ', $all_values) : NULL;
    }

    if (isset($value) && $encode) {
      $value = htmlspecialchars($value, ENT_QUOTES | ENT_XML1, 'UTF-8');
    }
    return $value;
  }

  /**
   * Add images fields to XML array.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
   *   Content of the recall.
   * @param string $recall_data_field_key
   *   The key of the field from which to get the images.
   *
   * @return array|null
   *   ['xml' => array, 'files' => array] Content of the recall's images fields
   *   and array of file names/paths.
   */
  private function getImagesFields(FieldableEntityInterface $entity, string $recall_data_field_key): ?array {
    $value = $entity->get($recall_data_field_key)->getValue();
    if (!isset($value) || count($value) === 0) {
      return NULL;
    }
    $fileIds = [];
    foreach ($value as $fileId) {
      $fileIds[] = $fileId['target_id'];
    }

    try {
      $files = $this->entityTypeManager()->getStorage('file')->loadMultiple($fileIds);
    }
    catch (\Throwable $exception) {
      return NULL;
    }

    $images = [];
    $uri = [];
    foreach ($files as $file) {
      if (!$file->status) {
        continue;
      }

      $uri[] = [
        'path' => $this->getValue($file, 'uri', FALSE, FALSE),
        'name' => $this->getValue($file, 'filename', FALSE, FALSE),
      ];

      $images[] = [
        'filename' => $this->getValue($file, 'filename'),
        'media_type' => $this->getValue($file, 'filemime'),
        // @todo Support custom alt text.
        'alt_text' => 'Image of ' . htmlspecialchars($entity->getTitle(), ENT_QUOTES | ENT_XML1, 'UTF-8'),
      ];
    }

    return [
      'xml' => $images,
      'files' => $uri,
    ];
  }

}
