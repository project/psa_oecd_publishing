<?php

namespace Drupal\psa_oecd_publishing\Form;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\psa_oecd_publishing\Controller\OecdMapperController;
use Drupal\psa_oecd_publishing\Controller\OecdPublishingController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list of recalls to be published to the OECD GlobalRecalls API.
 */
class OecdPublishingRecallsList extends FormBase {

  /**
   * Creates a OecdPublishingSettingsForm instance.
   *
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entity_field.manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager service.
   * @param \Drupal\psa_oecd_publishing\Controller\OecdPublishingController $oecdPublisher
   *   The psa_oecd_publishing.controller service.
   */
  public function __construct(
    protected EntityFieldManager $entityFieldManager,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected OecdPublishingController $oecdPublisher,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): object {
    return new static(
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('psa_oecd_publishing.controller'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'psa_oecd_publishing_publish_needed_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('psa_oecd_publishing.settings');
    $jurisdiction = $config->get('recall_jurisdiction');

    if (!$jurisdiction) {
      $this->messenger()->addWarning($this->t('OECD GlobalRecalls API module is not configured.'));
      return $form;
    }

    // Display a table of nodes that need to be published.
    $nodes = $this->oecdPublisher->getNodes();
    $rows = [];
    foreach ($nodes as $node) {
      $recallLanguages = $this->oecdPublisher->getRecallLanguages($node);

      $recall = $this->oecdPublisher->getRecall($node);
      $operation = $this->oecdPublisher->getRecallOperation($recall, psa_oecd_publishing_node_is_published($node));

      // Skip nodes that do not need any action.
      if (!$operation) {
        $this->logger('psa_oecd_publishing')->notice('Node @nodeId needs no operation; skipping.', ['@nodeId' => $node->id()]);
        $this->oecdPublisher->updateNodeStatus($node, TRUE);
        continue;
      }

      $row = [];
      $row['title'] = ['data' => ['#title' => $node->getTitle()]];
      $row[] = $this->oecdPublisher->getRecallId($node);
      $row[] = $node->toLink();
      $row[] = implode(', ', $recallLanguages);
      $row[] = OecdMapperController::getOperationName($operation);

      $rows[$node->id()] = $row;
    }

    $form['recalls_table'] = [
      '#type' => 'tableselect',
      '#empty' => $this->t('No recalls to publish.'),
      '#header' => [
        $this->t('Recall ID'),
        $this->t('Recall'),
        $this->t('Languages'),
        $this->t('API operation'),
      ],
      '#options' => $rows,
      '#attributes' => [
        'legend' => $this->t('There is one row for each recall with a checkbox in the first column. Select the recalls that you would like to publish.'),
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Publish selected to OECD GlobalRecalls portal'),
      // Disable when there are no nodes to publish.
      '#disabled' => !$rows,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Load the nodes that were displayed on the form screen.
    $nids = [];
    foreach ($form_state->getValue('recalls_table') as $nid => $selected) {
      if ($selected) {
        $nids[] = $nid;
      }
    }

    if ($nids) {
      $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($nids);
    }
    else {
      $this->messenger()->addError($this->t('No recalls were selected to be published.'));
      return;
    }

    // Publish the nodes (insert, update, delete).
    [$isSuccess, $status] = $this->oecdPublisher->publish(FALSE, $nodes);
    // Output status.
    if ($isSuccess) {
      $this->messenger()->addStatus($this->t(
        'Publishing to OECD completed. Nodes processed: @total. Succeeded: @passed. Failed: @failed. Check logs for details.',
        [
          '@total' => $status['total'],
          '@passed' => $status['passed'],
          '@failed' => $status['failed'],
        ]
      ));
    }
    else {
      $this->messenger()->addError($this->t('There was an error processing. Check logs for details.'));
    }
  }

}
