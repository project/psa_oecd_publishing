<?php

namespace Drupal\psa_oecd_publishing\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\psa_oecd_publishing\Controller\OecdMapperController;
use Drupal\psa_oecd_publishing\Controller\OecdPublishingController;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a OECD Publishing configuration form.
 *
 * @todo Add validation to prevent people from mapping the "Fields that must
 * exist on the recall content type".
 * @todo Add a check that everything is configured, perhaps integrating with
 * hook_requirements().
 * @todo Add check that the recalls View has the needed characteristics. The
 * View should include unpublished nodes so that they will be deleted from OECD.
 */
class OecdPublishingSettingsForm extends ConfigFormBase {
  use ImmutableConfigFormBaseTrait;

  // Mapping between field types from OecdMapperController::getFields() and
  // Drupal types.
  const FIELD_TYPE_MAPPING = [
    'bool' => 'boolean',
    'date' => 'datetime',
    'images' => 'image',
    'timestamp' => 'timestamp',
  ];

  /**
   * Creates a OecdPublishingSettingsForm instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config.factory service. Property of parent class.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entity_field.manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file_system service.
   * @param \Drupal\psa_oecd_publishing\Controller\OecdPublishingController $oecdPublisher
   *   The psa_oecd_publishing.controller service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    protected EntityFieldManager $entityFieldManager,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected FileSystemInterface $fileSystem,
    protected OecdPublishingController $oecdPublisher,
  ) {
    parent::__construct($configFactory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): object {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_field.manager'),
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('psa_oecd_publishing.controller'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'psa_oecd_publishing_settings_form';
  }

  /**
   * Get an array of the fields on the recall content type by type.
   *
   * These fields are used as options for field mapping. This also checks if the
   * content type has the minimum required fields for an OECD GlobalRecalls API
   * request. If not, it returned NULL.
   *
   * @param string|null $recall_data_content_type
   *   The content type to look for fields on.
   *
   * @return array|string
   *   An array of types each of which is an array of $field_id => $field_name.
   *   If not enough fields of the needed types, return a string error message.
   */
  public function getContentTypeFields(?string $recall_data_content_type): mixed {
    if (!$recall_data_content_type) {
      return NULL;
    }

    $recall_data_content_type_fields = [];
    // Provide node ID as a mappable field.
    $recall_data_content_type_fields['string']['id'] = $this->t('Node ID');
    $allowed_fields = ['title', 'body'];
    foreach ($this->entityFieldManager->getFieldDefinitions('node', $recall_data_content_type) as $field_id => $field) {
      // Include only fields: title, body, field_*.
      if (str_starts_with($field_id, 'field_') || in_array($field_id, $allowed_fields, TRUE)) {
        // Make a separate option list for some field types.
        $type = in_array($field->getType(), self::FIELD_TYPE_MAPPING, TRUE) ? $field->getType() : 'string';
        $recall_data_content_type_fields[$type][$field_id] = $field->getLabel();
      }
    }

    // Test if the content type has needed fields and return NULL if not.
    $types_needed = [
      'boolean' => 1,
      'datetime' => 1,
      'image' => 0,
      'string' => 2,
      'timestamp' => 1,
    ];
    foreach ($types_needed as $type => $number) {
      $recall_data_content_type_fields[$type] ??= [];
      if (count($recall_data_content_type_fields[$type]) < $number) {
        // Cast to string to ensure the return type indicates the error state.
        return (string) $this->t('Missing @type.', ['@type' => $type]);
      }
    }

    return $recall_data_content_type_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $form['oecd_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OECD GlobalRecalls API key'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigValue('oecd_api_key'),
    ];

    $form['oecd_api_use_production_host'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use OECD GlobalRecalls API production host'),
      '#description' => $this->t('When checked, use the production host of the OECD GlobalRecalls API; otherwise, use the testing host.'),
      '#default_value' => $this->getConfigValue('oecd_api_use_production_host'),
    ];

    $form['recall_jurisdiction'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Jurisdiction of recall'),
      '#description' => $this->t('ISO 3166 code of country where recall was published. Example: AU.'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigValue('recall_jurisdiction'),
    ];

    $recall_data_content_type = $this->getConfigValue('recall_data_content_type');
    $options = [];
    foreach ($this->entityTypeManager->getStorage('node_type')->loadMultiple() as $node_type_id => $node_type) {
      $options[$node_type_id] = $node_type->get('name');
    }
    $form['recall_data_content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Recall content type'),
      '#description' => $this->t('The content type that contains the recalls to be uploaded to the OECD GlobalRecalls portal. This must have at minimum the following fields: <ul>
        <li>1 boolean for the publish-needed status</li>
        <li>1 timestamp to store the published timestamp</li>
        <li>1 date for the OECD GlobalRecalls API <code>date</code> field</li>
        <li>2 strings for the OECD GlobalRecalls API <code>id</code> and <code>product_name</code> fields</li>
        </ul>
        Do not change this configuration once the system is in production. If this is changed, recalls already on the OECD GlobalRecalls portal will be left there with no easy method for removing or updating them. Likewise, removing any fields used below will cause it to stop working properly.'),
      // In testing, there may not be any content types.
      '#required' => (bool) $options,
      '#options' => $options,
      '#default_value' => $recall_data_content_type,
    ];

    $recall_data_content_type_fields = $this->getContentTypeFields($recall_data_content_type);
    if (!is_array($recall_data_content_type_fields)) {
      $recall_data_content_type_fields = NULL;
    }

    if ($recall_data_content_type_fields) {
      $form['recall_data_content_type_fields_needed'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Fields that must exist on the recall content type'),
      ];

      $form['recall_data_content_type_fields_needed']['oecd_publish_needed_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Recall content type publish-needed field'),
        '#description' => $this->t('This field is used to track if a recall needs to be published to the OECD GlobalRecalls portal. It is set to true when the recall is edited and false when it is published to the portal.'),
        '#required' => TRUE,
        '#empty_value' => '',
        '#options' => $recall_data_content_type_fields['boolean'],
        '#default_value' => $this->getConfigValue('oecd_publish_needed_field'),
      ];

      $form['recall_data_content_type_fields_needed']['oecd_publish_timestamp_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Recall content type published timestamp field'),
        '#description' => $this->t('This field is set to the current time when the recall is published to the OECD GlobalRecalls portal.'),
        '#required' => TRUE,
        '#empty_value' => '',
        '#options' => $recall_data_content_type_fields['timestamp'],
        '#default_value' => $this->getConfigValue('oecd_publish_timestamp_field'),
      ];
    }

    $form['recalls_view'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Recalls View'),
      '#description' => $this->t('Nodes in this View display will be published to the OECD GlobalRecalls portal if all of the following are true: <ul>
        <li>The node is of the recall content type configured above.</li>
        <li>The publish-needed field of the node is set to true.</li>
        <li>The recall ID field of the node is not empty.</li>
        </ul>
        To delete a recall from the OECD GlobalRecalls portal, it must appear in this View and be unpublished. Ensure the View does not filter out unpublished nodes. If the View is paged, only nodes on first page will be acted on. Run publishing again to act on the next page of nodes.'),
    ];

    $form['recalls_view']['recalls_view_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('View ID'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigValue('recalls_view_id'),
    ];

    $form['recalls_view']['recalls_view_display_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Display ID'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigValue('recalls_view_display_id'),
    ];

    // Mapping between OECD GlobalRecalls API fields and content type fields.
    $form['oecd_recall_field_mapping'] = [
      '#type' => 'table',
      '#caption' => $this->t('Field mapping: Configure the mapping between OECD GlobalRecalls API fields and recall content type fields.'),
      '#empty' => $this->t('After you choose and save a recall content type, the field mapping configuration will appear here.'),
      '#header' => [
        $this->t('OECD GlobalRecalls API field'),
        $this->t('Type'),
        $this->t('Recall content type field'),
      ],
    ];
    if ($recall_data_content_type_fields) {
      // Get a list of the fields in the OECD GlobalRecalls API.
      $oecd_recall_fields = OecdMapperController::getFields();
      // Get currently-saved mapping.
      $oecd_recall_field_mapping_config = $this->getConfigValue('oecd_recall_field_mapping');
      // Generate form controls for mapping between OECD GlobalRecalls API
      // fields and recall content type fields.
      foreach ($oecd_recall_fields as $oecd_field_key => $oecd_field_info) {
        // Skip non-mappable fields.
        if (!$oecd_field_info['mappable']) {
          continue;
        }
        // Determine the type of fields to display as options, default 'string'.
        $field_type = self::FIELD_TYPE_MAPPING[$oecd_field_info['type']] ?? 'string';
        // Create an ID to associate the label to the form control.
        $id = Html::getUniqueId('edit-oecd-field-' . $oecd_field_key);
        // Generate the form control and add it to the table.
        $form['oecd_recall_field_mapping'][$oecd_field_key] = [
          'label' => [
            '#type' => 'label',
            '#title' => $oecd_field_key,
            '#id' => FALSE,
            '#for' => $id,
            '#required' => $oecd_field_info['required'],
          ],
          'type' => [
            '#markup' => $oecd_field_info['type'],
          ],
          'select' => [
            '#type' => 'select',
            '#title' => $oecd_field_key,
            '#title_display' => 'attribute',
            '#id' => $id,
            '#required' => $oecd_field_info['required'],
            '#empty_value' => '',
            '#options' => $recall_data_content_type_fields[$field_type],
            '#default_value' => $oecd_recall_field_mapping_config[$oecd_field_key] ?? NULL,
          ],
        ];
      }
    }

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced configuration'),
    ];

    $form['advanced']['oecd_api_host_production'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OECD GlobalRecalls API production host'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigValue('oecd_api_host_production'),
    ];

    $form['advanced']['oecd_api_host_testing'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OECD GlobalRecalls API testing host'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigValue('oecd_api_host_testing'),
    ];

    $form['advanced']['oecd_export_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('File system path'),
      '#description' => $this->t('Drupal file system path to the directory where request files will be saved. Example: private://oecd_publishing'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigValue('oecd_export_path'),
    ];

    $form['advanced']['queue_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Queue name for async processing'),
      '#required' => TRUE,
      '#default_value' => $this->getConfigValue('queue_name'),
    ];

    $form['actions']['validate'] = [
      '#weight' => 100,
      '#type' => 'submit',
      '#value' => 'Validate API key',
      '#submit' => ['::validateApiKey'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // Validate oecd_export_path.
    if (!$this->fileSystem->prepareDirectory($form_state->getValue('oecd_export_path'), $this->fileSystem::CREATE_DIRECTORY)) {
      $form_state->setErrorByName('oecd_export_path', $this->t('Unable to create export path directory.'));
    }

    // Validate recall_jurisdiction.
    if (!OecdMapperController::isValidJurisdiction($form_state->getValue('recall_jurisdiction'))) {
      $form_state->setErrorByName('recall_jurisdiction', $this->t('The jurisdiction code is invalid.'));
    }

    // Validate recall_data_content_type.
    $recall_data_content_type = $form_state->getValue('recall_data_content_type');
    if ($recall_data_content_type) {
      $fields = $this->getContentTypeFields($recall_data_content_type);
      if (!is_array($fields)) {
        $form_state->setErrorByName('recall_data_content_type', $this->t('The recall content type does not include the fields needed for it to work with the OECD GlobalRecalls API. @message', ['@message' => $fields]));
      }
    }

    // Validate View.
    $recalls_view_id = $form_state->getValue('recalls_view_id');
    $view = Views::getView($recalls_view_id);
    if (!$view) {
      $form_state->setErrorByName('recalls_view_id', $this->t('This View does not exist.'));
    }
    // Validate the View display only if the View exists.
    else {
      $recalls_view_display_id = $form_state->getValue('recalls_view_display_id');
      if (!$view->setDisplay($recalls_view_display_id)) {
        $form_state->setErrorByName('recalls_view_display_id', $this->t('This View display does not exist.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    // Save individual config values.
    $config = $this->getEditableConfig();
    $fields_to_save = [
      'oecd_api_host_production',
      'oecd_api_host_testing',
      'oecd_api_key',
      'oecd_api_use_production_host',
      'oecd_export_path',
      'queue_name',
      'recall_data_content_type',
      'oecd_publish_needed_field',
      'oecd_publish_timestamp_field',
      'recall_jurisdiction',
      'recalls_view_display_id',
      'recalls_view_id',
    ];
    foreach ($fields_to_save as $field) {
      $config->set($field, $form_state->getValue($field));
    }

    // Save field mapping config.
    $oecd_recall_field_mapping = [];
    $oecd_recall_fields = OecdMapperController::getFields();
    foreach ($oecd_recall_fields as $oecd_field_key => $oecd_field_info) {
      if (!$oecd_field_info['mappable']) {
        continue;
      }
      $key = ['oecd_recall_field_mapping', $oecd_field_key, 'select'];
      $value = $form_state->getValue($key);
      if ($value) {
        $oecd_recall_field_mapping[$oecd_field_key] = $value;
      }
    }
    $config->set('oecd_recall_field_mapping', $oecd_recall_field_mapping);

    $config->save();

    $this->getLogger('psa_oecd_publishing')->notice('OECD GlobalRecalls API settings have been updated.');
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['psa_oecd_publishing.settings'];
  }

  /**
   * Form submission handler for validating API key.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function validateApiKey(array &$form, FormStateInterface $form_state): void {
    $response = $this->oecdPublisher->oecdApi->ping();
    if ($response['isSuccess']) {
      $this->messenger()->addStatus($this->t('API key validation was successful.'));
    }
    else {
      $this->messenger()->addWarning($this->t('API key validation failed. Error message: @error', ['@error' => $response['error']]));
    }
  }

}
