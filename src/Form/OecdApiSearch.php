<?php

namespace Drupal\psa_oecd_publishing\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\psa_oecd_publishing\Api\OecdApi;
use Drupal\psa_oecd_publishing\Api\OecdApiRecall;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list of recalls to be published to the OECD GlobalRecalls API.
 *
 * @todo Have it also search locally for a recall that matches. If it finds one,
 * there should be text explaning that to delete, the user should unpublish the
 * recall.
 */
class OecdApiSearch extends FormBase {

  /**
   * Creates a OecdPublishingSettingsForm instance.
   *
   * @param \Drupal\psa_oecd_publishing\Api\OecdApi $oecdApi
   *   The OECD API service.
   */
  public function __construct(
    protected OecdApi $oecdApi,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): object {
    return new static(
      $container->get('oecd_api.recall'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'oecd_api_search_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('psa_oecd_publishing.settings');
    $jurisdiction = $config->get('recall_jurisdiction');

    if (!$jurisdiction) {
      $this->messenger()->addWarning($this->t('OECD GlobalRecalls API module is not configured.'));
      return $form;
    }

    // Make this a GET form.
    $form_state->setMethod('GET');
    // Clean-up the query string.
    $form['#after_build'] = ['::afterBuild'];
    // Ensure the page output responds without caching.
    $form['#cache'] = [
      'max-age' => 0,
    ];

    $recall_id = $this->getRequest()->query->get('recall_id');

    if ($recall_id) {
      $rows = [];
      foreach (OecdApiRecall::$supportedLanguages as $lang) {
        $recall = new OecdApiRecall($lang, $jurisdiction, $recall_id);

        $recall_obj = $this->oecdApi->get($recall);

        if (!$recall_obj) {
          continue;
        }

        $recall_url = $this->oecdApi->getRecallUrl($recall, TRUE);
        $recall_link = Link::fromTextAndUrl($recall_obj->recall->id, $recall_url);

        $route_parameters = [
          'lang' => $recall->lang,
          'jurisdiction' => $recall->jurisdiction,
          'recall_id' => $recall->id,
        ];

        $rows[] = [
          $lang,
          $recall_link,
          $recall_obj->recall->date,
          $recall_obj->recall->{'product.name'},
          Link::createFromRoute($this->t('Delete'), 'psa_oecd_publishing.delete', $route_parameters),
        ];
      }

      $form['recalls_table'] = [
        '#type' => 'table',
        '#empty' => $this->t('No recalls found.'),
        '#header' => [
          $this->t('Language'),
          $this->t('Recall ID'),
          $this->t('Date'),
          $this->t('Product'),
          $this->t('Delete'),
        ],
        '#rows' => $rows,
      ];
    }

    $form['recall_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Recall ID'),
      '#required' => TRUE,
      '#default_value' => $recall_id,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      // Set empty so it does not get passed to the query string.
      '#name' => '',
    ];

    return $form;
  }

  /**
   * Custom #after_build removes elements from being submitted as GET variables.
   */
  public function afterBuild(array $element, FormStateInterface $form_state): array {
    unset($element['form_token']);
    unset($element['form_build_id']);
    unset($element['form_id']);
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
  }

}
