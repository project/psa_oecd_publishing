<?php

namespace Drupal\psa_oecd_publishing\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\psa_oecd_publishing\Api\OecdApiRecall;
use Drupal\psa_oecd_publishing\Controller\OecdMapperController;
use Drupal\psa_oecd_publishing\Controller\OecdPublishingController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a confirm form for deleting recalls from the OECD GlobalRecalls API.
 */
class OecdApiConfirmDelete extends ConfirmFormBase {

  /**
   * The recall to delete.
   *
   * @var \Drupal\psa_oecd_publishing\Api\OecdApiRecall
   */
  protected OecdApiRecall $recall;

  /**
   * Creates a OecdApiConfirmDelete instance.
   *
   * @param \Drupal\psa_oecd_publishing\Controller\OecdPublishingController $oecdPublishingController
   *   The psa_oecd_publishing.controller service.
   */
  public function __construct(
    protected OecdPublishingController $oecdPublishingController,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): object {
    return new static(
      $container->get('psa_oecd_publishing.controller'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Make a recall object to delete from the route parameters.
    $parameters = $this->getRouteMatch()->getParameters();
    $this->recall = new OecdApiRecall($parameters->get('lang'), $parameters->get('jurisdiction'), $parameters->get('recall_id'));

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'oecd_api_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText(): TranslatableMarkup {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('psa_oecd_publishing.search');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    $args = [
      '%lang' => $this->recall->lang,
      '%jurisdiction' => $this->recall->jurisdiction,
      '%recall_id' => $this->recall->id,
    ];
    return $this->t('Do you want to delete recall %lang/%jurisdiction/%recall_id from the OECD GlobalRecalls portal?', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Generate the request for the delete.
    $this->oecdPublishingController->mapper->getRequest(
      OecdMapperController::OECD_OPERATION_DELETE,
      $this->recall,
    );

    // The ID may have been altered by a hook. It must match what was sent in to
    // this form.
    $this->recall->request['id'] = $this->recall->id;

    // Creat the ZIP file.
    $archiveFileName = $this->oecdPublishingController->buildRequestArchive($this->recall->id, $this->recall->lang, $this->recall->getXml(), $this->recall->files);

    if (isset($archiveFileName)) {
      $response = NULL;
      $message = NULL;
      try {
        $response = $this->oecdPublishingController->oecdApi->post($archiveFileName);
      }
      catch (\Throwable $exception) {
        $message = $exception->getMessage();
      }

      if ($response) {
        $this->messenger()->addStatus($this->t('Recall delete request sent.'));
      }
      else {
        $this->messenger()->addError($this->t('Error sending recall delete request: @message', ['@message' => $message]));
      }
    }
    else {
      $this->messenger()->addError($this->t('Failed to create recall delete request.'));
    }
    $query = ['recall_id' => $this->recall->id];
    $form_state->setRedirect('psa_oecd_publishing.search', [], ['query' => $query]);
  }

}
