<?php

namespace Drupal\psa_oecd_publishing\Commands;

use Drupal\psa_oecd_publishing\Controller\OecdPublishingController;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml.
 */
class OecdPublishingCommands extends DrushCommands {

  /**
   * Constructs the OecdPublishingCommands object.
   *
   * @param \Drupal\psa_oecd_publishing\Controller\OecdPublishingController $oecdPublisher
   *   The OECD Publishing controller.
   */
  public function __construct(
    protected OecdPublishingController $oecdPublisher,
  ) {
  }

  /**
   * Execute command to publish recalls to OECD.
   *
   * @command psa:oecd-publish-recalls
   * @option async Use queue to publish recalls.
   * @usage drush psa:oecd-publish-recalls
   *   Publish recalls to OECD.
   * @usage drush psa:oecd-publish-recalls --async
   *   Publish recalls to OECD using queues to upload requests.
   */
  public function publish(array $options = ['async' => NULL]): void {
    $this->oecdPublisher->publish(isset($options['async']));
    $this->oecdPublisher->cleanup();
  }

  /**
   * Execute command to clean up requests.
   *
   * @command psa:oecd-clean-up
   * @usage drush psa:oecd-clean-up
   *   Clean up OECD request files older than 2 days
   * @usage drush psa:oecd-clean-up --all
   *   Clean up all OECD request files.
   */
  public function cleanup(array $options = ['all' => NULL]): void {
    $this->oecdPublisher->cleanup(isset($options['all']));
  }

}
