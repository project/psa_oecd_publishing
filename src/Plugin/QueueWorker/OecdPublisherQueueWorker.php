<?php

namespace Drupal\psa_oecd_publishing\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\psa_oecd_publishing\Controller\OecdPublishingController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides OECD publisher queue worker.
 *
 * @QueueWorker(
 *   id = "psa_oecd_publishing_queue",
 *   title = @Translation("Task Worker: OECD GlobalRecalls API Publisher"),
 *   cron = {"time" = 300}
 * )
 */
class OecdPublisherQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Constructs a new SpecificationController.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity_type.manager service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger.factory service.
   * @param \Drupal\psa_oecd_publishing\Controller\OecdPublishingController $publisherController
   *   The psa_oecd_publishing.controller service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LoggerChannelFactoryInterface $loggerFactory,
    protected OecdPublishingController $publisherController,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('logger.factory'),
      $container->get('psa_oecd_publishing.controller'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    if (empty($data['node_id']) || empty($data['archive_file_names'])) {
      $this->loggerFactory->get('psa_oecd_publishing')->error('Failed to process queue item. Empty data values.');
      return;
    }
    $nodeId = $data['node_id'];
    $archiveNames = $data['archive_file_names'];

    $isSuccess = FALSE;

    foreach ($archiveNames as $langcode => $archiveName) {
      $isSuccess = $this->publisherController->postRecall($archiveName, $nodeId, $langcode);
      if (!$isSuccess) {
        $context = [
          '@nodeId' => $nodeId,
          '@langcode' => $langcode,
        ];
        $this->loggerFactory->get('psa_oecd_publishing')->error('Failed to publish node @nodeId for language @langcode.', $context);
        break;
      }
    }

    if ($isSuccess) {
      $node = $this->entityTypeManager->getStorage('node')->load($nodeId);

      if (isset($node)) {
        $this->publisherController->updateNodeStatus($node);
      }
    }
  }

}
