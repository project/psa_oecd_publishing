<?php

namespace Drupal\Tests\psa_oecd_publishing\Unit;

use Drupal\psa_oecd_publishing\Api\OecdApiRecall;
use Drupal\psa_oecd_publishing\Controller\OecdMapperController;
use Drupal\Tests\UnitTestCase;

/**
 * Unit tests.
 *
 * @group PsaOecdPublishing
 */
class PsaOecdPublishingUnitTest extends UnitTestCase {

  /**
   * Test OecdMapperController.
   */
  public function testOecdMapperController(): void {
    // Test ::isValidJurisdiction().
    $test_values = [
      // Two-letter codes allowed.
      'CA' => TRUE,
      'AU' => TRUE,
      // Most strings are not allowed.
      'ca' => FALSE,
      'O' => FALSE,
      'FOO' => FALSE,
      // Most sub-national codes not allowed.
      'CA-ON' => FALSE,
      // Certain sub-national codes allowed under "ae".
      'AE-AZ' => TRUE,
      'AE-XX' => FALSE,
    ];
    foreach ($test_values as $input => $output) {
      $this->assertSame($output, OecdMapperController::isValidJurisdiction($input), $input . ' is ' . ($output ? 'valid' : 'invalid') . '.');
    }

    // Test ::isValidLanguage().
    $test_values = [
      // Allowed codes.
      'en' => TRUE,
      'fr' => TRUE,
      // All other values not allowed.
      'EN' => FALSE,
      'o' => FALSE,
      'foo' => FALSE,
    ];
    foreach ($test_values as $input => $output) {
      $this->assertSame($output, OecdMapperController::isValidLanguage($input), $input . ' is ' . ($output ? 'valid' : 'invalid') . '.');
    }
  }

  /**
   * Test OecdApiRecall.
   */
  public function testOecdApiRecall(): void {
    $id = $this->randomMachineName();

    // Test that properties are properly set.
    $recall = new OecdApiRecall('fr', 'CA', $id);
    $this->assertSame('fr', $recall->lang);
    $this->assertSame('CA', $recall->jurisdiction);
    $this->assertSame($id, $recall->id);

    $message = NULL;

    // Test validation in constructor.
    try {
      $recall = new OecdApiRecall('eng', 'CA', $id);
    }
    catch (\Exception $e) {
      $message = $e->getMessage();
    }
    $this->assertSame('Invalid language.', $message);

    try {
      $recall = new OecdApiRecall('fr', 'CAN', $id);
    }
    catch (\Exception $e) {
      $message = $e->getMessage();
    }
    $this->assertSame('Invalid jurisdiction.', $message);

    try {
      $recall = new OecdApiRecall('fr', 'CA', '');
    }
    catch (\Exception $e) {
      $message = $e->getMessage();
    }
    $this->assertSame('Invalid recall ID.', $message);

    // Test ::getXml().
    $this->assertNull($recall->getXml());
    // Set property and test XML output.
    $recall->request['title'] = $id;
    $xml = '<?xml version="1.0" encoding="UTF-8"?>
<recall xmlns="urn:recall-schema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="fr">
  <title>' . $id . '</title>
</recall>
';
    $this->assertSame($xml, $recall->getXml());
  }

}
