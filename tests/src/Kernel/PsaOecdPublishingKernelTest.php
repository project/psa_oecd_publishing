<?php

namespace Drupal\Tests\psa_oecd_publishing\Kernel;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Url;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\node\Entity\Node;
use Drupal\psa_oecd_publishing\Api\OecdApiRecall;
use Drupal\psa_oecd_publishing\Controller\OecdPublishingController;

/**
 * Kernel tests.
 *
 * @group PsaOecdPublishing
 */
class PsaOecdPublishingKernelTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'psa_oecd_publishing',
  ];

  /**
   * Test OecdMapperController.
   */
  public function testOecdMapperController(): void {
    // Instantiate OecdMapperController.
    $oecd_mapper_controller = \Drupal::service('psa_oecd_mapper.controller');

    $randomString = $this->randomString() . '<test> &amp;';

    // Create mock DateFormatter.
    $mock_date_formatter = $this->createMock(DateFormatter::class);
    $mock_date_formatter->expects($this->any())
      ->method('format')
      ->willReturn('0000-00-00');
    // Create mock field.
    $mock_field_item_list = $this->createMock(FieldItemList::class);
    $mock_field_item_list->expects($this->any())
      ->method('getValue')
      ->willReturn([$randomString, 'second-value']);
    $mock_field_item_list->expects($this->any())
      ->method('__get')
      ->with('date')
      ->willReturn($mock_date_formatter);
    // Create mock node containing mock field.
    $mock_node = $this->createMock(Node::class);
    $mock_node->expects($this->any())
      ->method('get')
      ->willReturn($mock_field_item_list);
    // Mock nid 1.
    $mock_node->expects($this->any())
      ->method('id')
      ->willReturn(1);
    // Mock title.
    $mock_node->expects($this->any())
      ->method('getTitle')
      ->willReturn('Title ' . $randomString);
    // Mock URL.
    $mock_url = $this->createMock(Url::class);
    $mock_url->expects($this->any())
      ->method('toString')
      ->willReturn('http://localhost/node/1');
    $mock_node->expects($this->any())
      ->method('toUrl')
      ->willReturn($mock_url);

    // Use same escaping for $randomString as in OecdMapperController.
    $randomString_escaped = htmlspecialchars($randomString, ENT_QUOTES | ENT_XML1, 'UTF-8');

    // Test Australia, English.
    $oecdApiRecall = new OecdApiRecall('en', 'AU', $randomString);
    $oecd_mapper_controller->getRequest('D', $oecdApiRecall);
    $xml = '<?xml version="1.0" encoding="UTF-8"?>
<recall xmlns="urn:recall-schema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="en">
  <edit_type>D</edit_type>
  <id>' . $randomString_escaped . '</id>
  <country_id>AU</country_id>
</recall>';
    $this->assertXmlStringEqualsXmlString($xml, $oecdApiRecall->getXml());
    $this->assertSame([], $oecdApiRecall->files);

    // Test Canada, French.
    $oecdApiRecall = new OecdApiRecall('fr', 'CA', $randomString);
    $oecd_mapper_controller->getRequest('D', $oecdApiRecall);
    $xml = '<?xml version="1.0" encoding="UTF-8"?>
<recall xmlns="urn:recall-schema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="fr">
  <edit_type>D</edit_type>
  <id>' . $randomString_escaped . '</id>
  <country_id>CA</country_id>
</recall>';
    $this->assertXmlStringEqualsXmlString($xml, $oecdApiRecall->getXml());
    $this->assertSame([], $oecdApiRecall->files);

    // Test Insert request.
    $fields = [
      // Mapping is ignored for 'id'; it reads it from $oecdApiRecal->id.
      'id' => 'id',
      'product_name' => 'title',
      'date' => 'field_date',
      'product_type' => 'field_product_type',
    ];
    $oecdApiRecall = new OecdApiRecall('fr', 'CA', $randomString);
    $oecd_mapper_controller->getRequest('I', $oecdApiRecall, $mock_node, $fields);
    $xml = '<?xml version="1.0" encoding="UTF-8"?>
<recall xmlns="urn:recall-schema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="fr">
  <edit_type>I</edit_type>
  <id>' . $randomString_escaped . '</id>
  <country_id>CA</country_id>
  <product_type>' . $randomString_escaped . ' ; second-value</product_type>
  <product_name>Title ' . $randomString_escaped . '</product_name>
  <URL>http://localhost/node/1</URL>
  <date>0000-00-00</date>
</recall>';
    $this->assertXmlStringEqualsXmlString($xml, $oecdApiRecall->getXml());
    $this->assertSame([], $oecdApiRecall->files);

    // Create mock field that returns empty except for date.
    $mock_field_empty = $this->createMock(FieldItemList::class);
    $mock_field_empty->expects($this->any())
      ->method('getValue')
      ->willReturn([]);
    $mock_field_empty->expects($this->any())
      ->method('__get')
      ->with('date')
      ->willReturn($mock_date_formatter);
    // Create mock node containing mock empty field.
    $mock_node = $this->createMock(Node::class);
    $mock_node->expects($this->any())
      ->method('get')
      ->willReturn($mock_field_empty);
    // Mock nid 1.
    $mock_node->expects($this->any())
      ->method('id')
      ->willReturn(1);
    // Mock title.
    $mock_node->expects($this->any())
      ->method('getTitle')
      ->willReturn('Title ' . $randomString);
    // Mock URL.
    $mock_url = $this->createMock(Url::class);
    $mock_url->expects($this->any())
      ->method('toString')
      ->willReturn('http://localhost/node/1');
    $mock_node->expects($this->any())
      ->method('toUrl')
      ->willReturn($mock_url);

    // Test Insert request with image element. This should not appear because it
    // it empty.
    $fields = [
      // Mapping is ignored for 'id'; it reads it from $oecdApiRecal->id.
      'id' => 'id',
      'product_name' => 'title',
      'date' => 'field_date',
      'image' => 'field_image',
    ];
    $oecdApiRecall = new OecdApiRecall('fr', 'CA', $randomString);
    $oecd_mapper_controller->getRequest('I', $oecdApiRecall, $mock_node, $fields);
    $xml = '<?xml version="1.0" encoding="UTF-8"?>
<recall xmlns="urn:recall-schema" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" lang="fr">
  <edit_type>I</edit_type>
  <id>' . $randomString_escaped . '</id>
  <country_id>CA</country_id>
  <product_name>Title ' . $randomString_escaped . '</product_name>
  <URL>http://localhost/node/1</URL>
  <date>0000-00-00</date>
</recall>';
    $this->assertXmlStringEqualsXmlString($xml, $oecdApiRecall->getXml());
    $this->assertSame([], $oecdApiRecall->files);
  }

  /**
   * Test OecdPublishingController.
   */
  public function testOecdPublishingController(): void {
    $this->enableModules([
      'language',
      'node',
      'user',
    ]);

    // Enable other languages.
    ConfigurableLanguage::createFromLangcode('de')->save();
    ConfigurableLanguage::createFromLangcode('fr')->save();

    // Test reading the recall language.
    $languages = [
      // DE is not supported, so ::getRecallLanguages() should return empty.
      'de' => [],
      'en' => ['en'],
      'fr' => ['fr'],
    ];
    foreach ($languages as $langcode => $lang_array) {
      $settings = [
        'title' => $this->randomString(),
        'type' => 'basic_page',
        'uid' => 0,
        'langcode' => $langcode,
      ];
      $node = Node::create($settings);

      $langcodes = OecdPublishingController::getRecallLanguages($node);
      $this->assertSame($lang_array, $langcodes);
    }
    // Add translations and test that only valid translations are returned.
    $node->addTranslation('en');
    $langcodes = OecdPublishingController::getRecallLanguages($node);
    $this->assertSame(['en', 'fr'], $langcodes);
    // Add DE, no change.
    $node->addTranslation('de');
    $langcodes = OecdPublishingController::getRecallLanguages($node);
    $this->assertSame(['en', 'fr'], $langcodes);
  }

  /**
   * Test OecdPublishingController.
   */
  public function testModuleFunctions(): void {
    $this->enableModules([
      'node',
      'user',
    ]);

    // Use existing 'sticky' field as the oecd_publish_needed_field.
    $config = \Drupal::service('config.factory')->getEditable('psa_oecd_publishing.settings');
    $config->set('oecd_publish_needed_field', 'sticky');
    $config->save();

    // Create a test node.
    $settings = [
      'title' => $this->randomString(),
      'type' => 'basic_page',
    ];
    $node = Node::create($settings);
    // Put a clone of the node on the $original property.
    $node_original = clone $node;
    $node->original = $node_original;

    // Test _psa_oecd_publishing_node_needs_oecd_publish() with all combinations
    // of input. If sticky-node and sticky-orig are different, never publish. If
    // They are the same, publish unless both node and orig are unpublished.
    // Also test psa_oecd_publishing_get_moderation_state() and
    // psa_oecd_publishing_node_is_published().
    $tests = [
      [
        'sticky-node' => FALSE,
        'sticky-orig' => FALSE,
        'pub-pub' => TRUE,
        'pub-unp' => TRUE,
        'unp-unp' => FALSE,
        'unp-pub' => TRUE,
      ],
      [
        'sticky-node' => TRUE,
        'sticky-orig' => TRUE,
        'pub-pub' => TRUE,
        'pub-unp' => TRUE,
        'unp-unp' => FALSE,
        'unp-pub' => TRUE,
      ],
      [
        'sticky-node' => FALSE,
        'sticky-orig' => TRUE,
        'pub-pub' => FALSE,
        'pub-unp' => FALSE,
        'unp-unp' => FALSE,
        'unp-pub' => FALSE,
      ],
      [
        'sticky-node' => TRUE,
        'sticky-orig' => FALSE,
        'pub-pub' => FALSE,
        'pub-unp' => FALSE,
        'unp-unp' => FALSE,
        'unp-pub' => FALSE,
      ],
    ];
    foreach ($tests as $test) {
      $node->setSticky($test['sticky-node']);
      $node_original->setSticky($test['sticky-orig']);
      // Node published, original published.
      $node->setPublished();
      $this->assertSame('published', psa_oecd_publishing_get_moderation_state($node));
      $this->assertTrue(psa_oecd_publishing_node_is_published($node));
      $this->assertSame($test['pub-pub'], _psa_oecd_publishing_node_needs_oecd_publish($node));
      // Node published, original unpublished.
      $node_original->setUnpublished();
      $this->assertSame($test['pub-unp'], _psa_oecd_publishing_node_needs_oecd_publish($node));
      // Node unpublished, original unpublished.
      $node->setUnpublished();
      $this->assertSame('unpublished', psa_oecd_publishing_get_moderation_state($node));
      $this->assertFalse(psa_oecd_publishing_node_is_published($node));
      $this->assertSame($test['unp-unp'], _psa_oecd_publishing_node_needs_oecd_publish($node));
      // Node unpublished, original published.
      $node_original->setPublished();
      $this->assertSame($test['unp-pub'], _psa_oecd_publishing_node_needs_oecd_publish($node));
    }

    // Test psa_oecd_publishing_get_moderation_state() using $moderation_state.
    $node->moderation_state = new \stdClass();
    $randomString = $this->randomString();
    $node->moderation_state->value = $randomString;
    $this->assertSame($randomString, psa_oecd_publishing_get_moderation_state($node));

    // Test early return in _psa_oecd_publishing_node_needs_oecd_publish().
    // With $skip_processing.
    $node->moderation_state->skip_processing = TRUE;
    $this->assertFalse(_psa_oecd_publishing_node_needs_oecd_publish($node));
    // With $bulk_update.
    $node->moderation_state->skip_processing = FALSE;
    $node->moderation_state->bulk_update = TRUE;
    $this->assertFalse(_psa_oecd_publishing_node_needs_oecd_publish($node));
  }

  /**
   * Test OecdApi.
   */
  public function testOecdApi(): void {
    $oecd_api_host['production'] = $this->randomMachineName() . '.production.example.org';
    $oecd_api_host['testing'] = $this->randomMachineName() . '.testing.example.org';
    $oecd_api_key = $this->randomMachineName();

    $config = \Drupal::service('config.factory')->getEditable('psa_oecd_publishing.settings');
    $config->set('oecd_api_host_production', $oecd_api_host['production']);
    $config->set('oecd_api_host_testing', $oecd_api_host['testing']);
    $config->set('oecd_api_key', $oecd_api_key);
    $config->save();

    $oecd_api_recall = \Drupal::service('oecd_api.recall');

    foreach (['testing', 'production'] as $host_type) {
      // Switch host second time through.
      if ($host_type === 'production') {
        $config->set('oecd_api_use_production_host', TRUE);
        $config->save();
      }

      // Test ::getPostUrl().
      $url = $oecd_api_recall->getPostUrl();
      $this->assertSame('https://' . $oecd_api_host[$host_type] . '/ws/import.xqy?apikey=' . $oecd_api_key, $url->toString());

      // Test ::getRecallUrl().
      $id = $this->randomMachineName();
      $recall = new OecdApiRecall('fr', 'CA', $id . ' SPACE');

      $url = $oecd_api_recall->getRecallUrl($recall);
      $this->assertSame('https://' . $oecd_api_host[$host_type] . '/ws/getrecall.xqy?uri=http%3A//PoliciesApplications.oecd.org/GlobalRecalls/Recall/FR/CA/' . $id . '%2520SPACE', $url->toString());

      $url = $oecd_api_recall->getRecallUrl($recall, TRUE);
      $this->assertSame('https://' . $oecd_api_host[$host_type] . '/#/recalls/http%3A%2F%2FPoliciesApplications.oecd.org%2FGlobalRecalls%2FRecall%2FFR%2FCA%2F' . $id . '%2520SPACE', $url->toString());
    }
  }

}
