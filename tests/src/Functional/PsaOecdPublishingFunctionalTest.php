<?php

namespace Drupal\Tests\psa_oecd_publishing\Functional;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;
use Drupal\views\Entity\View;

/**
 * Functional tests.
 *
 * @group PsaOecdPublishing
 */
class PsaOecdPublishingFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'datetime',
    'psa_oecd_publishing',
  ];

  /**
   * Tests.
   */
  public function test(): void {
    $users['admin'] = $this->createUser(['administer psa_oecd_publishing'], 'administer psa_oecd_publishing');
    $users['user'] = $this->createUser(['use psa_oecd_publishing'], 'use psa_oecd_publishing');

    // Anonymous user has no access.
    $this->drupalGet('admin/config/services/psa-oecd-publishing/publish-needed');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/config/services/psa-oecd-publishing/search');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/config/services/psa-oecd-publishing');
    $this->assertSession()->statusCodeEquals(403);

    // User with permissions has user access but not admin access.
    $this->drupalLogin($users['user']);
    $this->drupalGet('admin/config/services/psa-oecd-publishing/publish-needed');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('OECD GlobalRecalls API module is not configured.');
    $this->drupalGet('admin/config/services/psa-oecd-publishing/search');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('OECD GlobalRecalls API module is not configured.');
    $this->drupalGet('admin/config/services/psa-oecd-publishing');
    $this->assertSession()->statusCodeEquals(403);

    // Admin user has admin access but no user access.
    $this->drupalLogin($users['admin']);
    $this->drupalGet('admin/config/services/psa-oecd-publishing/publish-needed');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/config/services/psa-oecd-publishing/search');
    $this->assertSession()->statusCodeEquals(403);
    $this->drupalGet('admin/config/services/psa-oecd-publishing');
    $this->assertSession()->statusCodeEquals(200);

    // Test that the recall jurisdiction is blank.
    $recall_jurisdiction = $this->xpath('//input[@id="edit-recall-jurisdiction"]')[0]->getAttribute('value');
    $this->assertEmpty($recall_jurisdiction);

    // Create a View for validation of the view and display.
    $view = View::create(['id' => 'recalls-view-id']);
    $view->addDisplay('page', NULL, 'recalls-view-display-id');
    $view->save();

    // Validate API key, if it is configured in settings.testing.php as follows:
    // @code
    // $config['psa_oecd_publishing.settings']['oecd_api_key'] = 'THE-KEY';
    // @endcode
    // If the API key is configured, it will already appear on the config page.
    $oecd_api_key = $this->xpath('//input[@id="edit-oecd-api-key"]')[0]->getAttribute('value');

    // Save config.
    $edit = [
      'edit-recall-jurisdiction' => 'CA',
      'edit-recalls-view-id' => 'recalls-view-id',
      'edit-recalls-view-display-id' => 'recalls-view-display-id',
    ];
    // If the key is not configured, use a test value to allow saving the form.
    if (!$oecd_api_key) {
      $edit['edit-oecd-api-key'] = 'TEST_KEY';
    }

    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->pageTextContains('After you choose and save a recall content type, the field mapping configuration will appear here.');

    // Test that the recall jurisdiction was saved.
    $recall_jurisdiction = $this->xpath('//input[@id="edit-recall-jurisdiction"]')[0]->getAttribute('value');
    $this->assertSame($edit['edit-recall-jurisdiction'], $recall_jurisdiction);

    // If the API key is configured, validate that the key works on the API.
    if ($oecd_api_key) {
      $this->submitForm([], 'Validate API key');
      $this->assertSession()->pageTextContains('API key validation was successful.');
    }

    // Create Recall content type.
    $values = [
      'type' => 'recall',
      'name' => 'Recall',
    ];
    $recall_type = $this->createContentType($values);

    // Add published timestamp field to Recall content type.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_published_timestamp',
      'entity_type' => 'node',
      'type' => 'timestamp',
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $recall_type->get('type'),
    ]);
    $field->save();

    // Add date field to Recall content type.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_recall_date',
      'entity_type' => 'node',
      'type' => 'datetime',
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $recall_type->get('type'),
    ]);
    $field->save();

    // Reload config page so that content type selector shows new type.
    $this->drupalGet('admin/config/services/psa-oecd-publishing');
    // Save config with the recall type selected. Save will fail because of
    // missing boolean.
    $edit = [
      'edit-recall-data-content-type' => 'recall',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextNotContains('The configuration options have been saved.');
    $this->assertSession()->pageTextContains('After you choose and save a recall content type, the field mapping configuration will appear here.');
    $this->assertSession()->pageTextContains('The recall content type does not include the fields needed for it to work with the OECD GlobalRecalls API. Missing boolean.');

    // Add publish needed field to Recall content type.
    $fieldStorage = FieldStorageConfig::create([
      'field_name' => 'field_publish_needed',
      'entity_type' => 'node',
      'type' => 'boolean',
    ]);
    $fieldStorage->save();
    $field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => $recall_type->get('type'),
    ]);
    $field->save();

    // All fields should be in place, save will succeed.
    $this->submitForm([], 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->pageTextNotContains('After you choose and save a recall content type, the field mapping configuration will appear here.');

    // Configure the field mapping.
    $edit = [
      'edit-oecd-publish-needed-field' => 'field_publish_needed',
      'edit-oecd-publish-timestamp-field' => 'field_published_timestamp',
      'edit-oecd-field-id' => 'id',
      'edit-oecd-field-product-name' => 'title',
      'edit-oecd-field-hazard' => 'body',
      'edit-oecd-field-date' => 'field_recall_date',
      'edit-oecd-api-use-production-host' => TRUE,
      'edit-oecd-api-host-production' => $this->randomMachineName(),
      'edit-oecd-api-host-testing' => $this->randomMachineName(),
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertTrue((bool) $this->xpath('//input[@id="edit-oecd-api-use-production-host"]')[0]->getAttribute('checked'));
    $this->assertSame($edit['edit-oecd-api-host-production'], $this->xpath('//input[@id="edit-oecd-api-host-production"]')[0]->getAttribute('value'));
    $this->assertSame($edit['edit-oecd-api-host-testing'], $this->xpath('//input[@id="edit-oecd-api-host-testing"]')[0]->getAttribute('value'));

    // As user, view the empty published-needed page.
    $this->drupalLogin($users['user']);
    $this->drupalGet('admin/config/services/psa-oecd-publishing/publish-needed');
    $this->assertSession()->pageTextNotContains('OECD GlobalRecalls API module is not configured.');
    $this->assertSession()->pageTextContains('No recalls to publish.');

    // Create a Recall node.
    $values = [
      'title' => $this->randomString(),
      'type' => 'recall',
      'body' => [
        [
          'value' => $this->randomString(),
          'format' => filter_default_format(),
        ],
      ],
      'field_publish_needed' => TRUE,
      'field_published_timestamp' => 1660920764,
      'field_recall_date' => '2022-08-31',
    ];
    $recall_node = $this->createNode($values);
    $recall_node->save();

    $this->assertSame($values['title'], $recall_node->getTitle());
    $this->assertSame($values['body'][0]['value'], $recall_node->get('body')[0]->getValue()['value']);
    $this->assertSame($values['field_publish_needed'], $recall_node->get('field_publish_needed')->getValue()[0]['value']);
    $this->assertSame($values['field_published_timestamp'], $recall_node->get('field_published_timestamp')->getValue()[0]['value']);
    $this->assertSame($values['field_recall_date'], $recall_node->get('field_recall_date')->getValue()[0]['value']);

    // Publish-needed page should now show the new recall node.
    $this->drupalGet('admin/config/services/psa-oecd-publishing/publish-needed');
    $this->assertSession()->pageTextNotContains('No recalls to publish.');
    // Table cells.
    $rows = $this->getSession()->getPage()->findAll('xpath', '//table[@id="edit-recalls-table"]//tbody/tr');
    $this->assertCount(1, $rows);
    $tds = $rows[0]->findAll('xpath', '//td');
    $this->assertCount(5, $tds);
    $this->assertSame('1', $tds[1]->getText());
    $this->assertSame($values['title'], $tds[2]->getText());
    $this->assertSame('en', $tds[3]->getText());
    $this->assertSame('Insert', $tds[4]->getText());
    // Test that first column contains an 'input' element.
    $input = $tds[0]->findAll('xpath', '//input');
    $this->assertCount(1, $input);

    // Submit with no recalls selected.
    $this->submitForm([], 'Publish selected to OECD GlobalRecalls portal');
    $this->assertSession()->pageTextContains('No recalls were selected to be published.');
  }

}
